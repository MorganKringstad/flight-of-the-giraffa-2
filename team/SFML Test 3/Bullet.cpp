
#include "Bullet.h"
#include <SFML\Main.hpp>
#include <SFML\Window.hpp>
#include <SFML\System.hpp>
#include <SFML\Graphics.hpp>

Bullet::Bullet(sf::RectangleShape p_xBox,sf::Sprite p_SPrite, sf::RenderWindow &window)
{
	IsAlive = true;
	m_xBox=p_xBox;
	m_Sprite = p_SPrite;
	
	rect.setFillColor(sf::Color::Red);
	rect.setSize(sf::Vector2f(30, 15));
	mousePos2 = window.mapPixelToCoords(sf::Mouse::getPosition(window));
		
}
Bullet::~Bullet()
{

}

void Bullet::SetBulletRotation(float bulletRotation)
{
	m_BulletRotation = bulletRotation;
}

sf::RectangleShape Bullet::GetBox()
{
	return m_xBox;
}
sf::FloatRect Bullet::GetRect()
{
	m_Rect = rect.getGlobalBounds();
	return m_Rect;
}
sf::Sprite Bullet::GetSprite()
{
	
	return m_Sprite;
}
void Bullet::SetAngle(float newAngle)
{
	m_Angle = newAngle;
}

void Bullet::Update(float p_DeltaTime, sf::RenderWindow &window, sf::Vector2f playerPos)
{
	m_Sprite.setPosition(rect.getPosition());
	float angle2 = 1.0f;
	m_Sprite.setRotation(m_BulletRotation);
	rect.setRotation(m_BulletRotation);
	m_xBox.setRotation(m_BulletRotation);
	
	
	rect.move(cos(m_Angle)*20.0f, sin(m_Angle)*20.0f);
	
	
}

bool Bullet::GetIsAlive() { return IsAlive; }
void Bullet::SetIsAlive(bool Set_IsAlive) { IsAlive = Set_IsAlive; }
