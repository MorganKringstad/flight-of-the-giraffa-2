#pragma once

#include <SFML\Main.hpp>
#include <SFML\Window.hpp>
#include <SFML\System.hpp>
#include <SFML\Graphics.hpp>
//#include "EnemyBullet.h"
#include "SoundManager.h"
#include <SFML/Audio.hpp>

class EnemyCephalopods
{
public:
	
	EnemyCephalopods(sf::Vector2f position, sf::Sprite p_Sprite);
	~EnemyCephalopods();
	void Update(float p_DeltaTime);
	sf::Vector2f GetPosition();
	void SetPosition(sf::Vector2f new_Position);
	void SetFireAray(bool Set_FireAray);
	sf::RectangleShape GetBox();
	sf::FloatRect GetRect();
	sf::RectangleShape enemyBullet;
	//sf::RectangleShape GetEnemyBullet();
	bool isActive();
	int getDeathTimer();
	sf::Sprite GetSprite();
	bool GetFireAray();
	bool GetFireAngle();
	bool GetIsAlive();
	
	void SetStartAnimationDeath(bool Value);
	bool GetStartAnimationDeath();

	void normalShot();
	void draw(sf::RenderWindow* window); 

	enum enemyCephalopodsState
	{
		INACTIVE = 0,
		ACTIVE,
		DeathState
	};

	enemyCephalopodsState m_enemyCephalopodsState = ACTIVE;

	enemyCephalopodsState GetState();
	void SetState(enemyCephalopodsState p_eValue);
	

	bool IsAlive;
	void Death();
private:


	//EnemyBullet enemyBullet;
	SoundManager soundmanager;
	sf::Sprite m_Sprite;
//	ginny* ginny;
	sf::RectangleShape Rect_enemyCephalopods; // Need to change to a Texture.
	sf::Vector2f m_Position;
	sf::FloatRect m_rect;
	bool m_isActive;
	int m_deathTimer;
	int m_healLeft; // May not be needed.
	int m_shotTimer; // inteval timmer
	int m_move;
	int m_moveTimer; // inteval timmer
	float pi;
	bool FireAngle;
	bool FireAray;
	int m_InternalMoveTimer_1;
	int m_InternalMoveTimer_2;
	
	bool Start_animationDeath;
	bool Done_animationDeath;
	
};
