#include "EnemyCephalopods.h"
#include <SFML\Main.hpp>
#include <SFML\Window.hpp>
#include <SFML\System.hpp>
#include <SFML\Graphics.hpp>
#include "ginny.h"
#include "SoundManager.h"
//#include "EnemyBullet.h"


// This is the Enemy that Fire diagonal shots. hmm they maybe they can't see stright forwerde.

EnemyCephalopods::EnemyCephalopods(sf::Vector2f p_Position,sf::Sprite p_Sprite)
{
	

	//enemyBullet = EnemyBullet(p_xBox, p_SPrite, &window);
	m_Sprite = p_Sprite;

	

	
	
	m_Position = p_Position;
	m_isActive = true;
	m_deathTimer = 0;
	m_healLeft = 0;
	m_move = 0;
	m_moveTimer = 0;
	m_shotTimer = 0;
	IsAlive = true;
	m_Sprite.setPosition(m_Position);
	pi = 3.14159265359;
	FireAngle = false;
	FireAray = 0;
	Start_animationDeath = false;
	//Rect_enemyCephalopods.setTexture(m_Sprite.getTexture());

}

EnemyCephalopods::~EnemyCephalopods()
{
	
}

bool EnemyCephalopods::isActive()
{
	return 1;
}
sf::FloatRect EnemyCephalopods::GetRect()
{
	m_rect = m_Sprite.getGlobalBounds();
	return m_rect;
}

sf::Sprite EnemyCephalopods::GetSprite()
{
	return m_Sprite;
}

void EnemyCephalopods::Update(float p_DeltaTime)
{
	
	switch (m_enemyCephalopodsState)
	{
	case EnemyCephalopods::INACTIVE:
		//Check if EnemyCephalopods is on Screen Change State
		break;
	case EnemyCephalopods::ACTIVE:
		
		if (m_Sprite.getPosition().x <= 1980) // If The Enemy is in the Avatar Area
		{
			m_moveTimer++;
			if (m_moveTimer >= 50)
			{
				m_move = 7;
			}

			else if (m_moveTimer <= 50) // Does this work?
			{
				m_move = 6;
				//Rect_enemyCephalopods.move(-2, -2); Move left and down
			}

			if (m_moveTimer >= 100)
			{
				m_moveTimer = 0;
				//Reset
			}
		}
		else
		{
			m_move = 9;
		}


		//Shots
		m_shotTimer++;
		if (m_shotTimer == 50)
		{
			normalShot();
			m_shotTimer = 0;
			
		}
		
		///*if (m_Sprite.getPosition().x <= 0)
		//{
		//	m_Sprite.setPosition(1900, m_Position.y);
		//}*/
	/*	if (m_Sprite.getPosition().y <= 0)
		{
			m_Sprite.setPosition(m_Position.x, 1060);
		}*/
		//Movement
		if (m_move == 0)
		{
			m_Sprite.move(0, 0);
		}
		if (m_move == 1)
		{
			m_Sprite.move(0, 2);
		}
		if (m_move == 2)
		{
			m_Sprite.move(0, -2);
		}
		if (m_move == 3)
		{
			m_Sprite.move(2, 0);
		}
		if (m_move == 4)
		{
			m_Sprite.move(-2, 0);
		}
		if (m_move == 5)
		{
			m_Sprite.move(2, 2);
		}
		if (m_move == 6)
		{
			m_Sprite.move(-2, 2);
		}
		if (m_move == 7)
		{
			m_Sprite.move(-2, -2);
		}
		if (m_move == 8)
		{
			m_Sprite.move(2, -2);
		}
		if (m_move == 9)
		{
			m_Sprite.move(-4, 0);
		}
		//Movemet-end

		if (IsAlive == false)
		{
			m_enemyCephalopodsState = DeathState;
			//	m_Sprite.setPosition(1900, m_Position.y);
		}
		break;
	case EnemyCephalopods::DeathState:
		m_Sprite.setRotation(90);
		IsAlive = false;
		break;
	}
}


//sf::Vector2f EnemyCephalopods::SetPosition(sf::Vector2f new_Position)
//{
//	m_Position = new_Position;
//	return m_Position;
//}
sf::Vector2f EnemyCephalopods::GetPosition() 
{ 
	return m_Sprite.getPosition(); 
}

void EnemyCephalopods::SetStartAnimationDeath(bool Value)
{
	Start_animationDeath = Value;
}
bool EnemyCephalopods::GetStartAnimationDeath() { return Start_animationDeath; }
bool EnemyCephalopods::GetFireAray() { return FireAray; }
bool EnemyCephalopods::GetIsAlive() { return IsAlive; }
bool EnemyCephalopods::GetFireAngle() { return FireAngle; }
void EnemyCephalopods::SetFireAray(bool Set_FireAray) { FireAray = Set_FireAray; }
void EnemyCephalopods::SetPosition(sf::Vector2f new_Position) { m_Position = new_Position; }

void EnemyCephalopods::Death()
{
	m_rect = sf::RectangleShape(sf::Vector2f(0, 0)).getGlobalBounds();
	//Rect_enemyCephalopods.setSize(sf::Vector2f(0, 0));
}
sf::RectangleShape EnemyCephalopods::GetBox()
{
	return Rect_enemyCephalopods;
}
void EnemyCephalopods::normalShot()
{
	
	//soundmanager.PlayEffect(coin);
	FireAray = 1;

	if (FireAngle == false)
	{
		FireAngle = true;
	}

	else
	{
		FireAngle = false;
	}

	//if (IsAlive == true)
	//{
	//	if (FireAngle1 >= 1) // Up
	//	{
	//		enemyBullet.rect.setPosition(GetPosition());
	//		enemyBullet.SetAnagle((2.9) *(180 / pi));
	//		FireAray = 1;
	//		//enemybulletArray.push_back(enemyBullet);
	//		FireAngle1 = 0;
	//	}

	//	else
	//	{
	//		enemyBullet.rect.setPosition(GetPosition());
	//		enemyBullet.SetAnagle((-2.9)*(180 / pi));
	//		//enemybulletArray.push_back(enemyBullet);
	//		FireAray = 1;
	//		FireAngle1++;

	//	}
}
//sf::RectangleShape EnemyCephalopods::GetEnemyBullet()
//{
//	return enemyBullet;
//}

void EnemyCephalopods::draw(sf::RenderWindow* window)
{
	window->draw(Rect_enemyCephalopods);
}


EnemyCephalopods::enemyCephalopodsState EnemyCephalopods::GetState() { return m_enemyCephalopodsState; }
void EnemyCephalopods::SetState(enemyCephalopodsState p_eValue) { m_enemyCephalopodsState = p_eValue; }

int EnemyCephalopods::getDeathTimer() { return m_deathTimer; }