#pragma once

#include <SFML\Main.hpp>
#include <SFML\Window.hpp>
#include <SFML\System.hpp>
#include <SFML\Graphics.hpp>
#include "SoundManager.h"
#include <SFML/Audio.hpp>

class EnemyVaranidaes
{
public:
	void SetPatternes(bool p_eValue);
	bool GetPatternes();

	EnemyVaranidaes(sf::Vector2f position, sf::Sprite p_Sprite);
	~EnemyVaranidaes();
	void Update(float p_DeltaTime);
	sf::Vector2f GetPosition();
	void SetPosition(sf::Vector2f new_Position);
	void SetFireAray(bool Set_FireAray);
	void SetHealthLeft(int Set_Health);
	sf::RectangleShape GetBox();
	sf::FloatRect GetRect();
	sf::RectangleShape enemyBullet;
	bool isActive();
	int getDeathTimer();
	sf::Sprite GetSprite();
	void SetTexture(sf::Texture New_texture);
	bool GetFireAray();
	bool GetIsAlive();
	int GetHealth();

	void SetStartAnimationDeath(bool Value);
	bool GetStartAnimationDeath();

	void normalShot();
	void draw(sf::RenderWindow* window);



	enum EnemyVaranidaesState
	{
		INACTIVE = 0,
		ACTIVE,
		DeathState
	};

	EnemyVaranidaesState m_EnemyVaranidaesState = ACTIVE;

	EnemyVaranidaesState GetState();
	void SetState(EnemyVaranidaesState p_eValue);

	bool IsAlive;
	void Death();
private:



	SoundManager soundmanager;
	sf::Sprite m_Sprite;

	sf::RectangleShape Rect_EnemyVaranidaes;
	sf::Vector2f m_Position;
	sf::FloatRect m_rect;
	bool m_isActive;
	int m_deathTimer;
	int m_healLeft; // May not be needed.
	int m_shotTimer; // inteval timmer
	int m_move;
	int m_moveTimer; // inteval timmer
	float pi;
	int FireAngle1;
	bool FireAray;
	int m_InternalMoveTimer_1;
	int m_InternalMoveTimer_2;
	bool move_patterns = true;
	sf::Texture EnemyVarTextureDamge;

	bool Start_animationDeath;
	bool Done_animationDeath;
};
