#pragma once
#include <SFML\Main.hpp>
#include <SFML\Window.hpp>
#include <SFML\System.hpp>
#include <SFML\Graphics.hpp>
#include "ginny.h"

class TylerBullets
{
public:

	TylerBullets(sf::RectangleShape p_xBox, sf::RenderWindow &window, sf::Sprite p_sprite);
	~TylerBullets();
	sf::RectangleShape GetBox();
	void Update(float p_DeltaTime, sf::RenderWindow &window, sf::Vector2f tylerPos);
	sf::FloatRect GetRect();
	sf::Sprite GetSprite();
	void SetAngle(float newAngle);
	sf::Vector2f mousePos2;
	sf::RectangleShape rect;
	bool GetIsAlive();
	void SetIsAlive(bool Set_IsAlive);
private:

	sf::Sprite m_Sprite;
	sf::FloatRect m_Rect;
	sf::Vector2f m_TylerPos;
	sf::RectangleShape m_xBox;
	float m_Angle;
	bool IsAlive;


};