#include <SFML\Main.hpp>
#include <SFML\Window.hpp>
#include <SFML\System.hpp>
#include <SFML\Graphics.hpp>
#include "Asteroid.h"

Asteroid::Asteroid(sf::Sprite iSprite, sf::Vector2f p_Pos)
{
	
	m_Sprite = iSprite;
	m_Position = p_Pos;
	m_Sprite.setPosition (m_Position);
	m_Sprite.setOrigin(80, 80);

}
Asteroid::~Asteroid()
{

}

void Asteroid::SetPos(sf::Vector2f newPos)
{
	m_Position = newPos;
}
sf::Vector2f Asteroid::GetPos()
{
	return m_Sprite.getPosition();
}
sf::Sprite Asteroid::GetSprite()
{
	return m_Sprite;
}

void Asteroid::Update(float p_deltaTime)
{
	m_Sprite.move(-1, 0);

}
sf::FloatRect Asteroid::GetRect()
{

	m_Rect = m_Sprite.getGlobalBounds();
	return m_Rect;

}

