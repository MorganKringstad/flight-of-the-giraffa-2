#pragma once
#include <SFML\Audio.hpp>
#include <string.h>
#include <map>
#include <list>

class SoundManager
{
public:
	SoundManager();
	~SoundManager();

	//sf::SoundBuffer Buffer;
	//sf::Sound sound;
	sf::Music music;
	
	//void loadEffect(std::string p_loadEffectPath);
	void loadEffect();
	//void PlayEffect(std::string p_EffectPath);

	void LoadMusic(std::string p_EffectPath);
	void PlayMusic(std::string p_EffectPath);

	void PlayEffect(const std::string& );
	void update();

	void SetLoop(bool value);
	bool GetLoop();
	

private:
	
	sf::Sound m_sound;
	std::string m_loadEffect;
	std::string p_loadEffectPath;
	std::string p_effectName;
	std::string m_effectName;
	std::string setEffectName;

	std::map<std::string, sf::Sound> m_SFXMap;

	std::map<std::string, sf::Music> m_musicMap;
	




	std::list<sf::Sound> vSounds_;
	sf::SoundBuffer buffercoin;
	sf::SoundBuffer bufferplop;
	sf::SoundBuffer bufferGainPower;
	sf::SoundBuffer bufferPause;
	sf::SoundBuffer bufferjetpack;
	sf::SoundBuffer bufferenemyDeath;
	sf::SoundBuffer bufferginnyTakingDamage;
	sf::SoundBuffer bufferkometExplosion;
	sf::SoundBuffer bufferRainbowActivate;
	sf::SoundBuffer buffershieldActivate;
	sf::SoundBuffer buffertylerTakingDamage;
	sf::SoundBuffer bufferHoveringMeny;
	sf::SoundBuffer bufferNoPowerUp;
	sf::SoundBuffer bufferGttingHit;
	sf::SoundBuffer bufferclick;
	bool loops;

};

