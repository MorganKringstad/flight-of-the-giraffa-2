#pragma once
#include "ginny.h"
class ginny;

class GameState
{
public:
	GameState();
	~GameState();
	void Enter();
	void Exit();
	bool Update(float p_fDeltaTime);
private:
	
	ginny* m_pxGinny;
};
