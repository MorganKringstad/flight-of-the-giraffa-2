#include <SFML\Main.hpp>
#include <SFML\Window.hpp>
#include <SFML\System.hpp>
#include <SFML\Graphics.hpp>
#include "Bullet.h"
#include"ginny.h"
#include "EnemyCephalopods.h"
#include "EnemyVaranidaes.h"
#include "EnemyBullet.h"
#include <math.h>
#include "AnimatedSprite.hpp"
#include "Menu.h"
#include "Options.h"
#include <fstream>
#include <vector>
#include <SFML\Audio.hpp>
#include "Comet.h"
#include "TylerBullets.h"
#include <string>
//#include <iostream>
#include "SoundManager.h"
#include "Asteroid.h"
#include <string>
#include <fstream>
#include <string>
#include <sstream>
#include <cstring>
#include <cstdio>
#include "HighScore.h"
using namespace std;
	
#if !defined(NDEBUG)
#pragma comment(lib, "sfml-main-d.lib")
#pragma comment(lib, "sfml-window-d.lib")
#pragma comment(lib, "sfml-system-d.lib")
#pragma comment(lib, "sfml-graphics-d.lib")
#pragma comment(lib, "sfml-audio-d.lib")
#else
#pragma comment(lib, "sfml-main.lib")
#pragma comment(lib, "sfml-window.lib")
#pragma comment(lib, "sfml-system.lib")
#pragma comment(lib, "sfml-graphics.lib")
#pragma comment(lib, "sfml-audio.lib")
#endif

struct HitText { sf::Text text; sf::Vector2f position; float ttl; };
std::vector<HitText*> m_hitText;
sf::Font font;

void crateHitText(int point, sf::Vector2f position)
{
	HitText* newHitText = new HitText;
	m_hitText.push_back(newHitText);
	newHitText->ttl = 70.f;

	auto itHitText = m_hitText.begin();
	while (itHitText != m_hitText.end())
	{
		if ((*itHitText)->ttl < 0)
		{
			(*itHitText)->text.setFont(font);
			(*itHitText)->text.setString(std::to_string(point) + "p");
			(*itHitText)->text.setPosition(position);
			(*itHitText)->ttl = 70.f;
			m_hitText.erase(m_hitText.end() - 1);
			delete newHitText;
			break;
		}
		else
		{
			newHitText->text.setFont(font);
			newHitText->text.setString(std::to_string(point) + "p");
			newHitText->text.setPosition(position);
			newHitText->ttl = 70.f;
			break;
		}
	}
}


int main(int argc, char** argv)
{
	std::vector<EnemyCephalopods*> SpawnPointsCep;
	std::vector<EnemyVaranidaes*> SpawnPointsVar;
	
	
	

	SoundManager* soundManager;
	soundManager = new  SoundManager;

	soundManager->loadEffect();
	soundManager->PlayMusic("../sound/music/menustuffv2.ogg");

	enum Estate{GAMESTATE,MENUSTATE,WINSTATE,LOSESTATE, HIGHSCORESTATE};
	Estate gameEstate = MENUSTATE;
	sf::RenderWindow window(sf::VideoMode(1920, 1080), "Giraffa!", sf::Style::Fullscreen | sf::Style::Close);
	window.setFramerateLimit(60);
	Menu menu(window.getSize().x, window.getSize().y);
	Options options(window.getSize().x, window.getSize().y);

	//ARRAY OCH ITERATOR
	std::vector<Comet*> cometArray;
	std::vector<Comet>::const_iterator itercomets;
	sf::RectangleShape crosshair(sf::Vector2f(10, 10));
	std::vector<Bullet>::const_iterator iterBullets;
	std::vector<Bullet> bulletArray;
	std::vector<EnemyBullet>::const_iterator iterenemyBullets;
	std::vector<EnemyBullet> enemybulletArray;
	std::vector<TylerBullets>::const_iterator iterTBullets;
	std::vector<TylerBullets> tbulletArray;


	std::vector<Asteroid*> AsterArray;
	std::vector<Asteroid>::const_iterator iterAsteroid;
	HighScore oScore1, oScore2;


	//User Input
	sf::String userInput1;
	sf::String userInput2;
	sf::String userInput3;

	// shieldPowerUp
	bool shieldPickUpBool = false;
	bool shieldUsedBool=false;
	bool shield = false;
	sf::Time shieldTimer;
	sf::Clock shieldClock;
	sf::Texture ShieldPowerUp;
	sf::Texture ShieldPickUp;
	sf::Texture HudShield;
	ShieldPickUp.loadFromFile("../textures/shieldPickUp.png");
	HudShield.loadFromFile("../textures/shield.png");
	sf::Sprite hudShield(HudShield);
	sf::Texture tylerHead;
	tylerHead.loadFromFile("../textures/tylerhead.png");
	sf::Sprite tylerHeadSprite(tylerHead);

	sf::Texture TylerHud;
	TylerHud.loadFromFile("../textures/tylerhud.png");
	sf::Sprite tylerHud(TylerHud);

	sf::Texture EnemyBulletTexture;
	sf::Texture EnemyBullet2Texture;

	EnemyBulletTexture.loadFromFile("../textures/enemybullet2.png");
	EnemyBullet2Texture.loadFromFile("../textures/enemybullet1.png");

	sf::Sprite EnemyBulletSprite(EnemyBulletTexture);
	
	bool pause= false;

	sf::RectangleShape EnemybulletPBox = sf::RectangleShape(sf::Vector2f(20, 5));

	float EnemyBulletSpeed = 3.f;
	bool rainbowPowerUsed=false;
	bool rainbowPowerActive = false;
	bool NoPowerUpSound = false;
	int NoPowerUptimer = 0;
	int rainbowPowerUpUsed=0;
	int EnemybulletTimer = 0;
	float scale = 0;
	int rotation = 0;
	int winTimer = 0;
	bool keypressed0 = false;
	bool keypressed1 = false;
	bool keyreturn = false;
	font.loadFromFile("../textures/TOMOPRG_.TTF");
	sf::Text ScoreText;
	ScoreText.setFont(font);
	int X_pos = 0;
	int Y_pos = 0;
	int Xc_pos = 0;
	int Yc_pos = 0;

	float pi = 3.14159265359;
	EnemyBullet enemybullet(EnemybulletPBox, EnemyBulletSprite, window);
	int Score = 0;
	float mp = 1; //multi stats for Score
	int mpTtl = 0; // timeToLive for multi.

	sf::Text mpText;
	mpText.setFont(font);
	mpText.setScale(0.8, 0.8);

	// SPRITE OCH TEXTUR
	sf::Texture Cometex;
	Cometex.loadFromFile("../textures/meteor_animation.png");
	sf::Sprite cometsprt(Cometex, sf::IntRect(0, 0, 160, 160));

	sf::Texture AsterTex1;
	sf::Texture AsterTex2;
	sf::Texture AsterTex3;
	AsterTex1.loadFromFile("../textures/asteroid1.png");
	AsterTex2.loadFromFile("../textures/asteroid2.png");
	AsterTex3.loadFromFile("../textures/asteroid3.png");
	sf::Sprite AsterSprt1(AsterTex1);
	/*sf::Sprite AsterSprt2(AsterTex2);
	sf::Sprite AsterSprt3(AsterTex3);*/
	
	sf::Texture caget;
	sf::Texture Hud;
	sf::Texture CrossHairT;
	sf::Texture GinnyAnimation;
	sf::Texture RainbowPowerUpAnimation;
	sf::Texture GinnyHead;
	sf::Texture EnemyCepTexture;
	sf::Texture EnemyVarTexture;
	sf::Texture EnemyVarTextureDamge;
	sf::Texture GinnyTexture;
	sf::Texture BulletTexture;
	sf::Texture Background;
	sf::Texture BG_Stars1;
	sf::Texture BG_Stars2;
	sf::Texture BG_Stars3;
	sf::Texture Menu;
	sf::Texture Win;
	sf::Texture PowerUp;
	sf::Texture Lose;
	sf::Texture Rainbow;

	sf::Texture HallOfFame;
	sf::Texture tBullet;
	sf::Texture GinnyDamaged;
	sf::Texture GinnyHeadDamage;
	sf::Texture ShieldTexture;

	sf::Texture tutorialMove;
	sf::Texture tutorialLeft;
	sf::Texture tutorialRight;
	sf::Texture tutorialSpace;
	tutorialMove.loadFromFile("../textures/WASD.png");
	tutorialLeft.loadFromFile("../textures/Bl�skjut.png");
	tutorialRight.loadFromFile("../textures/Rosaskjut.png");
	tutorialSpace.loadFromFile("../textures/SPACE.png");
	sf::Sprite tutorialMoveSprite(tutorialMove);
	sf::Sprite tutorialLeftSprite(tutorialLeft);
	sf::Sprite tutorialRightSprite(tutorialRight);
	sf::Sprite tutorialSpaceSprite(tutorialSpace);
	bool tutorialMoveActiveW = true;
	bool tutorialMoveActiveA = true;
	bool tutorialMoveActiveS = true;
	bool tutorialMoveActiveD = true;
	bool tutorialLeftActive = true;
	bool tutorialRightActive = true;
	bool tutorialSpaceActive = false;
	bool tutorialSpaceActiveOnce = false;

	ShieldTexture.loadFromFile("../textures/shieldani.png");
	GinnyHeadDamage.loadFromFile("../textures/ginnyheaddamage.png");
	tBullet.loadFromFile("../textures/projectile_t.png");
	GinnyDamaged.loadFromFile("../textures/ginny-damage-animation.png");

	sf::Texture PauseTexture;

	bool PlayingRainbowSound = false;

	HallOfFame.loadFromFile("../textures/highscorescreen.png");
	Hud.loadFromFile("../textures/hud.png");
	CrossHairT.loadFromFile("../textures/crosshair.png");
	RainbowPowerUpAnimation.loadFromFile("../textures/power_upani.png");
	GinnyHead.loadFromFile("../textures/ginnyhead.png");
	GinnyAnimation.loadFromFile("../textures/ginny-idle-animation.png");
	Rainbow.loadFromFile("../textures/rainbow.png");
	Lose.loadFromFile("../textures/lose.png");
	Win.loadFromFile("../textures/win.png");
	Background.loadFromFile("../textures/background.png");
	BG_Stars1.loadFromFile("../textures/parallax_1.png");
	BG_Stars2.loadFromFile("../textures/parallax_2.png");
	BG_Stars3.loadFromFile("../textures/parallax_3.png");
	EnemyCepTexture.loadFromFile("../textures/Varanidaes.png");
	EnemyVarTexture.loadFromFile("../textures/Cephalopods.png");
	EnemyVarTextureDamge.loadFromFile("../textures/CephalopodsTemp.png");
	PauseTexture.loadFromFile("../textures/game_paused_temp.png");
	GinnyTexture.loadFromFile("../textures/ginny2.png");
	BulletTexture.loadFromFile("../textures/bullet.png");
	Menu.loadFromFile("../textures/menu.png");
	PowerUp.loadFromFile("../textures/power_up.png");
	
	bool tylerLives = true;
	bool PayingSound = false;
	sf::Sprite cagesprt(caget);
	sf::Sprite HallOfFameSPRT(HallOfFame);
	sf::Sprite CrossHair(CrossHairT);
	sf::Sprite ginnyHead(GinnyHead);
	sf::Sprite rainbow(Rainbow);
	sf::Sprite win(Win);
	sf::Sprite lose(Lose);
	sf::Sprite bulletSprite(BulletTexture);
	sf::Sprite tbulletSprite(tBullet);
	sf::Sprite backgroundSprite(Background);
	backgroundSprite.setPosition(0, 0);
	sf::Sprite backgroundSprite2(Background);
	backgroundSprite2.setPosition(1920, 0);
	sf::Sprite BG_StarsSprite1(BG_Stars1);
	BG_StarsSprite1.setPosition(0, 0);
	sf::Sprite BG_StarsSprite2(BG_Stars2);
	BG_StarsSprite2.setPosition(0, 0);
	sf::Sprite BG_StarsSprite3(BG_Stars3);
	BG_StarsSprite3.setPosition(0, 0);
	sf::Sprite BG_StarsSprite1copy(BG_Stars1);
	BG_StarsSprite1copy.setPosition(1920, 0);
	sf::Sprite BG_StarsSprite2copy(BG_Stars2);
	BG_StarsSprite2copy.setPosition(1920, 0);
	sf::Sprite BG_StarsSprite3copy(BG_Stars3);
	BG_StarsSprite3copy.setPosition(1920, 0);
	sf::Sprite PauseSprite(PauseTexture);
	sf::Sprite ginnySprite(GinnyTexture);
	sf::Sprite enemyCepSprite(EnemyCepTexture);
	sf::Sprite enemyVarSprite(EnemyVarTexture);
	sf::Sprite hud(Hud);
	sf::Sprite menuSprite(Menu);
	sf::Sprite powerUp(PowerUp);

	sf::RectangleShape bulletPBox = sf::RectangleShape(sf::Vector2f(20, 5));
	sf::CircleShape player(10.f);
	sf::Vector2f playerPos(490, 490);
	sf::Vector2f shotPos(500, 500);
	float bulletSpeed = 3.f;
	sf::Vector2f v(0, 0);
	player.setPosition(playerPos);
	sf::RectangleShape playerRect(sf::Vector2f(100,107));
	sf::RectangleShape rainbowPowerUpRect(sf::Vector2f(90,90));
	sf::RectangleShape shieldPowerUpRect(sf::Vector2f(90, 90));

	//ANIMATION
	sf::Texture TylerIdle;
	TylerIdle.loadFromFile("../textures/tyleranimation.png");
	Animation tylerIdle;
	tylerIdle.setSpriteSheet(TylerIdle);
	tylerIdle.addFrame(sf::IntRect(0, 0, 95, 108));
	tylerIdle.addFrame(sf::IntRect(95, 0, 95, 108));
	tylerIdle.addFrame(sf::IntRect(190, 0, 95, 108));
	tylerIdle.addFrame(sf::IntRect(285, 0, 95, 108));
	tylerIdle.addFrame(sf::IntRect(380, 0, 95, 108));
	tylerIdle.addFrame(sf::IntRect(475, 0, 95, 108));
	tylerIdle.addFrame(sf::IntRect(570, 0, 95, 108));
	tylerIdle.addFrame(sf::IntRect(665, 0, 95, 108));
	Animation* currentTylerAnimation = &tylerIdle;
	AnimatedSprite animatedTylerSpriteIdle(sf::seconds(0.1), true, false);

	sf::Texture TylerDamaged;
	TylerDamaged.loadFromFile("../textures/tylerdamaged.png");
	Animation tylerDamaged;
	tylerDamaged.setSpriteSheet(TylerDamaged);
	tylerDamaged.addFrame(sf::IntRect(0, 0, 95, 108));
	tylerDamaged.addFrame(sf::IntRect(95, 0, 95, 108));
	tylerDamaged.addFrame(sf::IntRect(190, 0, 95, 108));
	tylerDamaged.addFrame(sf::IntRect(285, 0, 95, 108));
	tylerDamaged.addFrame(sf::IntRect(380, 0, 95, 108));
	tylerDamaged.addFrame(sf::IntRect(475, 0, 95, 108));
	tylerDamaged.addFrame(sf::IntRect(570, 0, 95, 108));
	tylerDamaged.addFrame(sf::IntRect(665, 0, 95, 108));
	tylerDamaged.addFrame(sf::IntRect(760, 0, 95, 108));
	tylerDamaged.addFrame(sf::IntRect(855, 0, 95, 108));
	tylerDamaged.addFrame(sf::IntRect(950, 0, 95, 108));
	tylerDamaged.addFrame(sf::IntRect(1045, 0, 95, 108));
	AnimatedSprite animatedTylerDamagedSprite(sf::seconds(0.1), true, false);

	sf::Texture TylerDeath;
	TylerDeath.loadFromFile("../textures/tyler_death.png");
	Animation tylerDeath;
	tylerDeath.setSpriteSheet(TylerDeath);
	tylerDeath.addFrame(sf::IntRect(0, 0, 200, 200));
	tylerDeath.addFrame(sf::IntRect(200, 0, 200, 200));
	tylerDeath.addFrame(sf::IntRect(400, 0, 200, 200));
	tylerDeath.addFrame(sf::IntRect(600, 0, 200, 200));
	tylerDeath.addFrame(sf::IntRect(800, 0, 200, 200));
	tylerDeath.addFrame(sf::IntRect(1000, 0, 200, 200));
	tylerDeath.addFrame(sf::IntRect(1200, 0, 200, 200));
	tylerDeath.addFrame(sf::IntRect(1400, 0, 200, 200));
	tylerDeath.addFrame(sf::IntRect(1600, 0, 200, 200));
	tylerDeath.addFrame(sf::IntRect(1800, 0, 200, 200));
	tylerDeath.addFrame(sf::IntRect(2000, 0, 200, 200));
	tylerDeath.addFrame(sf::IntRect(2200, 0, 200, 200));
	tylerDeath.addFrame(sf::IntRect(0, 0, 0, 0));
	tylerDeath.addFrame(sf::IntRect(0, 0, 0, 0));
	Animation* currentTylerDeathAnimation = &tylerDeath;
	AnimatedSprite animatedTylerDeathSprite(sf::seconds(0.1), true, false);
	bool TylerDeathAnimationPlaying = false;

	sf::Texture ComExplAnimation;
	ComExplAnimation.loadFromFile("../textures/meteor_animation.png");
	Animation CometExplo;
	CometExplo.setSpriteSheet(ComExplAnimation);
	CometExplo.addFrame(sf::IntRect(0, 0, 160, 160));
	CometExplo.addFrame(sf::IntRect(160, 0, 160, 160));
	CometExplo.addFrame(sf::IntRect(320, 0, 160, 160));
	CometExplo.addFrame(sf::IntRect(480, 0, 160, 160));
	CometExplo.addFrame(sf::IntRect(640, 0, 160, 160));
	CometExplo.addFrame(sf::IntRect(800, 0, 160, 160));
	CometExplo.addFrame(sf::IntRect(960, 0, 160, 160));
	CometExplo.addFrame(sf::IntRect(1120, 0, 160, 160));
	CometExplo.addFrame(sf::IntRect(1280, 0, 160, 160));
	CometExplo.addFrame(sf::IntRect(1440, 0, 160, 160));
	CometExplo.addFrame(sf::IntRect(1600, 0, 160, 160));
	CometExplo.addFrame(sf::IntRect(1760, 0, 160, 160));
	CometExplo.addFrame(sf::IntRect(0, 0, 0, 0));
	Animation* currentCometExpl = &CometExplo;
	AnimatedSprite AnimatedCometExplosion(sf::seconds(0.1), true, false);
	AnimatedCometExplosion.setOrigin(80, 80);


	Animation shieldAnimation;
	shieldAnimation.setSpriteSheet(ShieldTexture);
	shieldAnimation.addFrame(sf::IntRect(0, 0, 135, 135));
	shieldAnimation.addFrame(sf::IntRect(135, 0, 135, 135));
	shieldAnimation.addFrame(sf::IntRect(270, 0, 135, 135));
	shieldAnimation.addFrame(sf::IntRect(405, 0, 135, 135));
	shieldAnimation.addFrame(sf::IntRect(540, 0, 135, 135));
	shieldAnimation.addFrame(sf::IntRect(675, 0, 135, 135));
	shieldAnimation.addFrame(sf::IntRect(810, 0, 135, 135));
	shieldAnimation.addFrame(sf::IntRect(945, 0, 135, 135));
	Animation* currentShieldAnimation = &shieldAnimation;
	AnimatedSprite animatedShieldSprite(sf::seconds(1.6), true, false);
	bool IsPlayinganimatedShieldSprite = false;

	Animation TylerShieldAnimation;
	TylerShieldAnimation.setSpriteSheet(ShieldTexture);
	TylerShieldAnimation.addFrame(sf::IntRect(0, 0, 135, 135));
	TylerShieldAnimation.addFrame(sf::IntRect(135, 0, 135, 135));
	TylerShieldAnimation.addFrame(sf::IntRect(270, 0, 135, 135));
	TylerShieldAnimation.addFrame(sf::IntRect(405, 0, 135, 135));
	TylerShieldAnimation.addFrame(sf::IntRect(540, 0, 135, 135));
	TylerShieldAnimation.addFrame(sf::IntRect(675, 0, 135, 135));
	TylerShieldAnimation.addFrame(sf::IntRect(810, 0, 135, 135));
	TylerShieldAnimation.addFrame(sf::IntRect(945, 0, 135, 135));
	Animation* TylercurrentShieldAnimation = &TylerShieldAnimation;
	AnimatedSprite TyleranimatedShieldSprite(sf::seconds(1.6), true, false);

	Animation rainbowPowerUpAnimation;
	rainbowPowerUpAnimation.setSpriteSheet(RainbowPowerUpAnimation);
	rainbowPowerUpAnimation.addFrame(sf::IntRect(0, 0, 90, 90));
	rainbowPowerUpAnimation.addFrame(sf::IntRect(90, 0, 90, 90));
	rainbowPowerUpAnimation.addFrame(sf::IntRect(180, 0, 90, 90));
	rainbowPowerUpAnimation.addFrame(sf::IntRect(270, 0, 90, 90));
	rainbowPowerUpAnimation.addFrame(sf::IntRect(360, 0, 90, 90));
	rainbowPowerUpAnimation.addFrame(sf::IntRect(450, 0, 90, 90));
	rainbowPowerUpAnimation.addFrame(sf::IntRect(540, 0, 90, 90));
	rainbowPowerUpAnimation.addFrame(sf::IntRect(630, 0, 90, 90));
	rainbowPowerUpAnimation.addFrame(sf::IntRect(720, 0, 90, 90));
	rainbowPowerUpAnimation.addFrame(sf::IntRect(810, 0, 90, 90));
	rainbowPowerUpAnimation.addFrame(sf::IntRect(900, 0, 90, 90));
	rainbowPowerUpAnimation.addFrame(sf::IntRect(990, 0, 90, 90));
	Animation* currentRainbowPowerUpAnimation = &rainbowPowerUpAnimation;
	AnimatedSprite animatedRainbowSpritePowerUp(sf::seconds(0.1), true, false);
	animatedRainbowSpritePowerUp.setPosition(-200, -200);

	Animation shieldPowerUpAnimation;
	shieldPowerUpAnimation.setSpriteSheet(ShieldPickUp);
	shieldPowerUpAnimation.addFrame(sf::IntRect(0, 0, 90, 90));
	shieldPowerUpAnimation.addFrame(sf::IntRect(90, 0, 90, 90));
	shieldPowerUpAnimation.addFrame(sf::IntRect(180, 0, 90, 90));
	shieldPowerUpAnimation.addFrame(sf::IntRect(270, 0, 90, 90));
	shieldPowerUpAnimation.addFrame(sf::IntRect(360, 0, 90, 90));
	shieldPowerUpAnimation.addFrame(sf::IntRect(450, 0, 90, 90));
	shieldPowerUpAnimation.addFrame(sf::IntRect(540, 0, 90, 90));
	shieldPowerUpAnimation.addFrame(sf::IntRect(630, 0, 90, 90));
	shieldPowerUpAnimation.addFrame(sf::IntRect(720, 0, 90, 90));
	shieldPowerUpAnimation.addFrame(sf::IntRect(810, 0, 90, 90));
	shieldPowerUpAnimation.addFrame(sf::IntRect(900, 0, 90, 90));
	shieldPowerUpAnimation.addFrame(sf::IntRect(990, 0, 90, 90));
	Animation* currentShieldPowerUpAnimation = &shieldPowerUpAnimation;
	AnimatedSprite animatedShieldSpritePowerUp(sf::seconds(0.1), true, false);
	animatedShieldSpritePowerUp.setPosition(-200, -200);

	Animation powerUpAnimation;
	powerUpAnimation.setSpriteSheet(RainbowPowerUpAnimation);
	powerUpAnimation.addFrame(sf::IntRect(0, 0, 90, 90));
	powerUpAnimation.addFrame(sf::IntRect(90, 0, 90, 90));
	powerUpAnimation.addFrame(sf::IntRect(180, 0, 90, 90));
	powerUpAnimation.addFrame(sf::IntRect(270, 0, 90, 90));
	powerUpAnimation.addFrame(sf::IntRect(360, 0, 90, 90));
	powerUpAnimation.addFrame(sf::IntRect(450, 0, 90, 90));
	powerUpAnimation.addFrame(sf::IntRect(540, 0, 90, 90));
	powerUpAnimation.addFrame(sf::IntRect(630, 0, 90, 90));
	powerUpAnimation.addFrame(sf::IntRect(720, 0, 90, 90));
	powerUpAnimation.addFrame(sf::IntRect(810, 0, 90, 90));
	powerUpAnimation.addFrame(sf::IntRect(900, 0, 90, 90));
	powerUpAnimation.addFrame(sf::IntRect(990, 0, 90, 90));
	Animation* currentPowerUpAnimation = &powerUpAnimation;
	AnimatedSprite animatedSpritePowerUp(sf::seconds(0.1), true, false);
	animatedSpritePowerUp.setPosition(-200, -200);
	animatedSpritePowerUp.setOrigin(45, 45);


	Animation ginnyIdle;
	ginnyIdle.setSpriteSheet(GinnyAnimation);
	ginnyIdle.addFrame(sf::IntRect(0, 0, 125, 130));
	ginnyIdle.addFrame(sf::IntRect(125, 0, 125, 130));
	ginnyIdle.addFrame(sf::IntRect(250, 0, 125, 130));
	ginnyIdle.addFrame(sf::IntRect(375, 0, 125, 130));
	ginnyIdle.addFrame(sf::IntRect(500, 0, 125, 130));
	ginnyIdle.addFrame(sf::IntRect(625, 0, 125, 130));
	ginnyIdle.addFrame(sf::IntRect(750, 0, 125, 130));
	ginnyIdle.addFrame(sf::IntRect(875, 0, 125, 130));
	AnimatedSprite animatedSpriteGinny(sf::seconds(0.1), true, false);

	Animation ginnyDamaged;
	ginnyDamaged.setSpriteSheet(GinnyDamaged);
	ginnyDamaged.addFrame(sf::IntRect(0, 0, 125, 130));
	ginnyDamaged.addFrame(sf::IntRect(125, 0, 125, 130));
	ginnyDamaged.addFrame(sf::IntRect(250, 0, 125, 130));
	ginnyDamaged.addFrame(sf::IntRect(375, 0, 125, 130));
	ginnyDamaged.addFrame(sf::IntRect(500, 0, 125, 130));
	ginnyDamaged.addFrame(sf::IntRect(625, 0, 125, 130));
	ginnyDamaged.addFrame(sf::IntRect(750, 0, 125, 130));
	ginnyDamaged.addFrame(sf::IntRect(875, 0, 125, 130));
	ginnyDamaged.addFrame(sf::IntRect(1000, 0, 125, 130));
	ginnyDamaged.addFrame(sf::IntRect(1125, 0, 125, 130));
	ginnyDamaged.addFrame(sf::IntRect(1250, 0, 125, 130));
	ginnyDamaged.addFrame(sf::IntRect(1375, 0, 125, 130));
	AnimatedSprite animatedSpriteGinnyDamaged(sf::seconds(0.1), true, false);

	Animation ginnyHeadDamaged;
	ginnyHeadDamaged.setSpriteSheet(GinnyHeadDamage);
	ginnyHeadDamaged.addFrame(sf::IntRect(0, 0, 36, 43));
	ginnyHeadDamaged.addFrame(sf::IntRect(36, 0, 36, 43));
	ginnyHeadDamaged.addFrame(sf::IntRect(72, 0, 36, 43));
	ginnyHeadDamaged.addFrame(sf::IntRect(108, 0, 36, 43));
	ginnyHeadDamaged.addFrame(sf::IntRect(144, 0, 36, 43));
	ginnyHeadDamaged.addFrame(sf::IntRect(180, 0, 36, 43));
	ginnyHeadDamaged.addFrame(sf::IntRect(216, 0, 36, 43));
	ginnyHeadDamaged.addFrame(sf::IntRect(252, 0, 36, 43));
	ginnyHeadDamaged.addFrame(sf::IntRect(288, 0, 36, 43));
	ginnyHeadDamaged.addFrame(sf::IntRect(324, 0, 36, 43));
	ginnyHeadDamaged.addFrame(sf::IntRect(360, 0, 36, 43));
	ginnyHeadDamaged.addFrame(sf::IntRect(396, 0, 36, 43));
	Animation* currentGinnyHeadAnimation = &ginnyHeadDamaged;
	AnimatedSprite animatedSpriteGinnyHeadDamaged(sf::seconds(0.1), true, false);
	
	sf::Texture EnemyCepAnimationTexture;
	EnemyCepAnimationTexture.loadFromFile("../textures/enemy_cep_blink_test.png");

	Animation enemy_cep_blink;
	enemy_cep_blink.setSpriteSheet(EnemyCepAnimationTexture);
	enemy_cep_blink.addFrame(sf::IntRect(0, 0, 100, 128));
	enemy_cep_blink.addFrame(sf::IntRect(0, 0, 100, 128));
	enemy_cep_blink.addFrame(sf::IntRect(0, 0, 100, 128));
	enemy_cep_blink.addFrame(sf::IntRect(0, 0, 100, 128));
	enemy_cep_blink.addFrame(sf::IntRect(0, 0, 100, 128));
	enemy_cep_blink.addFrame(sf::IntRect(0, 0, 100, 128));
	enemy_cep_blink.addFrame(sf::IntRect(0, 0, 100, 128));
	enemy_cep_blink.addFrame(sf::IntRect(0, 0, 100, 128));
	enemy_cep_blink.addFrame(sf::IntRect(0, 0, 100, 128));
	enemy_cep_blink.addFrame(sf::IntRect(0, 0, 100, 128));
	enemy_cep_blink.addFrame(sf::IntRect(0, 0, 100, 128));
	enemy_cep_blink.addFrame(sf::IntRect(0, 0, 100, 128));
	enemy_cep_blink.addFrame(sf::IntRect(0, 0, 100, 128));
	enemy_cep_blink.addFrame(sf::IntRect(0, 0, 100, 128));
	enemy_cep_blink.addFrame(sf::IntRect(0, 0, 100, 128));
	enemy_cep_blink.addFrame(sf::IntRect(0, 0, 100, 128));
	enemy_cep_blink.addFrame(sf::IntRect(0, 0, 100, 128));
	enemy_cep_blink.addFrame(sf::IntRect(0, 0, 100, 128));
	enemy_cep_blink.addFrame(sf::IntRect(0, 0, 100, 128));
	enemy_cep_blink.addFrame(sf::IntRect(0, 0, 100, 128));
	enemy_cep_blink.addFrame(sf::IntRect(0, 0, 100, 128));
	enemy_cep_blink.addFrame(sf::IntRect(0, 0, 100, 128));
	enemy_cep_blink.addFrame(sf::IntRect(0, 0, 100, 128));
	enemy_cep_blink.addFrame(sf::IntRect(0, 0, 100, 128));
	enemy_cep_blink.addFrame(sf::IntRect(0, 0, 100, 128));
	enemy_cep_blink.addFrame(sf::IntRect(0, 0, 100, 128));
	enemy_cep_blink.addFrame(sf::IntRect(0, 0, 100, 128));
	enemy_cep_blink.addFrame(sf::IntRect(0, 0, 100, 128));
	enemy_cep_blink.addFrame(sf::IntRect(0, 0, 100, 128));
	enemy_cep_blink.addFrame(sf::IntRect(0, 0, 100, 128));
	enemy_cep_blink.addFrame(sf::IntRect(100, 0, 100, 128));
	enemy_cep_blink.addFrame(sf::IntRect(200, 0, 100, 128));
	enemy_cep_blink.addFrame(sf::IntRect(300, 0, 100, 128));
	enemy_cep_blink.addFrame(sf::IntRect(400, 0, 100, 128));
	enemy_cep_blink.addFrame(sf::IntRect(500, 0, 100, 128));
	enemy_cep_blink.addFrame(sf::IntRect(600, 0, 100, 128));
	enemy_cep_blink.addFrame(sf::IntRect(700, 0, 100, 128));
	enemy_cep_blink.addFrame(sf::IntRect(800, 0, 100, 128));
	enemy_cep_blink.addFrame(sf::IntRect(900, 0, 100, 128));
	enemy_cep_blink.addFrame(sf::IntRect(1000, 0, 100, 128));
	Animation* currentEnemyCepAnimation = &enemy_cep_blink;
	AnimatedSprite EnemyCepAnimation(sf::seconds(2), true, false);

	sf::Texture EnemyCepDeathAnimationTexture;
	EnemyCepDeathAnimationTexture.loadFromFile("../textures/enemy_cep_death.png");

	Animation enemy_cep_Death;
	enemy_cep_Death.setSpriteSheet(EnemyCepDeathAnimationTexture);
	enemy_cep_Death.addFrame(sf::IntRect(0, 0, 200, 200));
	enemy_cep_Death.addFrame(sf::IntRect(200, 0, 200, 200));
	enemy_cep_Death.addFrame(sf::IntRect(400, 0, 200, 200));
	enemy_cep_Death.addFrame(sf::IntRect(600, 0, 200, 200));
	enemy_cep_Death.addFrame(sf::IntRect(800, 0, 200, 200));
	enemy_cep_Death.addFrame(sf::IntRect(1000, 0, 200, 200));
	enemy_cep_Death.addFrame(sf::IntRect(1200, 0, 200, 200));
	enemy_cep_Death.addFrame(sf::IntRect(1600, 0, 200, 200));
	enemy_cep_Death.addFrame(sf::IntRect(1800, 0, 200, 200));
	enemy_cep_Death.addFrame(sf::IntRect(2000, 0, 200, 200));
	enemy_cep_Death.addFrame(sf::IntRect(2200, 0, 200, 200));
	enemy_cep_Death.addFrame(sf::IntRect(2400, 0, 200, 200));
	enemy_cep_Death.addFrame(sf::IntRect(2600, 0, 200, 200));
	enemy_cep_Death.addFrame(sf::IntRect(2800, 0, 200, 200));
	enemy_cep_Death.addFrame(sf::IntRect(3000, 0, 200, 200));
	enemy_cep_Death.addFrame(sf::IntRect(3200, 0, 200, 200));
	enemy_cep_Death.addFrame(sf::IntRect(0, 0, 0, 0));
	Animation* currentEnemyCepDetahAnimation = &enemy_cep_Death;
	AnimatedSprite EnemyCepDeathAnimation(sf::seconds(0.1), true, false);

	sf::Texture EnemyVarDeathAnimationTexture;
	EnemyVarDeathAnimationTexture.loadFromFile("../textures/enemy_var_death.png");

	Animation enemy_var_Death;
	enemy_var_Death.setSpriteSheet(EnemyVarDeathAnimationTexture);
	enemy_var_Death.addFrame(sf::IntRect(0, 0, 200, 200));
	enemy_var_Death.addFrame(sf::IntRect(200, 0, 200, 200));
	enemy_var_Death.addFrame(sf::IntRect(400, 0, 200, 200));
	enemy_var_Death.addFrame(sf::IntRect(600, 0, 200, 200));
	enemy_var_Death.addFrame(sf::IntRect(800, 0, 200, 200));
	enemy_var_Death.addFrame(sf::IntRect(1000, 0, 200, 200));
	enemy_var_Death.addFrame(sf::IntRect(1200, 0, 200, 200));
	enemy_var_Death.addFrame(sf::IntRect(1400, 0, 200, 200));
	enemy_var_Death.addFrame(sf::IntRect(1600, 0, 200, 200));
	enemy_var_Death.addFrame(sf::IntRect(1800, 0, 200, 200));
	enemy_var_Death.addFrame(sf::IntRect(2000, 0, 200, 200));
	enemy_var_Death.addFrame(sf::IntRect(2200, 0, 200, 200));
	enemy_var_Death.addFrame(sf::IntRect(2400, 0, 200, 200));
	enemy_var_Death.addFrame(sf::IntRect(2600, 0, 200, 200));
	enemy_var_Death.addFrame(sf::IntRect(2800, 0, 200, 200));
	enemy_var_Death.addFrame(sf::IntRect(3000, 0, 200, 200));
	enemy_var_Death.addFrame(sf::IntRect(0, 0, 0, 0));
	Animation* currentEnemyvarDetahAnimation = &enemy_var_Death;
	AnimatedSprite EnemyvarDeathAnimation(sf::seconds(0.1), true, false);

	
	sf::Texture EnemyVarAnimationTexture;
	EnemyVarAnimationTexture.loadFromFile("../textures/enemyvarmove.png");

	Animation enemy_var;
	enemy_var.setSpriteSheet(EnemyVarAnimationTexture);
	enemy_var.addFrame(sf::IntRect(0, 0, 110, 105));
	enemy_var.addFrame(sf::IntRect(110, 0, 110, 105));
	enemy_var.addFrame(sf::IntRect(220, 0, 110, 105));
	enemy_var.addFrame(sf::IntRect(330, 0, 110, 105));
	enemy_var.addFrame(sf::IntRect(440, 0, 110, 105));
	enemy_var.addFrame(sf::IntRect(550, 0, 110, 105));
	enemy_var.addFrame(sf::IntRect(660, 0, 110, 105));
	enemy_var.addFrame(sf::IntRect(770, 0, 110, 105));
	enemy_var.addFrame(sf::IntRect(880, 0, 110, 105));
	Animation* currentEnemyvarAnimation = &enemy_var;
	AnimatedSprite EnemyvarhAnimation(sf::seconds(2.5), true, false);

	sf::Texture EnemyVarAnimationdmgTexture;
	EnemyVarAnimationdmgTexture.loadFromFile("../textures/enemyvardmg.png");

	Animation enemyVarDmg;
	enemyVarDmg.setSpriteSheet(EnemyVarAnimationdmgTexture);
	enemyVarDmg.addFrame(sf::IntRect(0, 0, 110, 105));
	enemyVarDmg.addFrame(sf::IntRect(110, 0, 110, 105));
	enemyVarDmg.addFrame(sf::IntRect(220, 0, 110, 105));
	enemyVarDmg.addFrame(sf::IntRect(330, 0, 110, 105));
	enemyVarDmg.addFrame(sf::IntRect(440, 0, 110, 105));
	enemyVarDmg.addFrame(sf::IntRect(550, 0, 110, 105));
	enemyVarDmg.addFrame(sf::IntRect(660, 0, 110, 105));
	enemyVarDmg.addFrame(sf::IntRect(770, 0, 110, 105));
	enemyVarDmg.addFrame(sf::IntRect(880, 0, 110, 105));
	Animation* currentEnemyVarDmgAnimation = &enemyVarDmg;
	AnimatedSprite EnemyvardmgAnimation(sf::seconds(3), true, true);
	bool vardmgplay = false;

	sf::Texture firetailTexture;
	firetailTexture.loadFromFile("../textures/firetail.png");

	Animation firetail;
	firetail.setSpriteSheet(firetailTexture);
	firetail.addFrame(sf::IntRect(0, 0, 154, 211));
	firetail.addFrame(sf::IntRect(154, 0, 154, 211));
	firetail.addFrame(sf::IntRect(308, 0, 154, 211));
	firetail.addFrame(sf::IntRect(462, 0, 154, 211));
	firetail.addFrame(sf::IntRect(616, 0, 154, 211));
	firetail.addFrame(sf::IntRect(770, 0, 154, 211));
	firetail.addFrame(sf::IntRect(924, 0, 154, 211));
	firetail.addFrame(sf::IntRect(1078, 0, 154, 211));
	firetail.addFrame(sf::IntRect(1232, 0, 154, 211));
	firetail.addFrame(sf::IntRect(1386, 0, 154, 211));
	Animation* currentfiretailAnimation = &firetail;
	AnimatedSprite firetailAnimation(sf::seconds(1), true, false);
	firetailAnimation.setOrigin(77, 190);
	firetailAnimation.setRotation(80);
	firetailAnimation.setScale(0.8, 0.9);

	sf::Texture GreenBulletHitT;
	GreenBulletHitT.loadFromFile("../textures/green-projectile-blow-up.png");

	Animation GreenBulletHit;
	GreenBulletHit.setSpriteSheet(GreenBulletHitT);
	GreenBulletHit.addFrame(sf::IntRect(0, 0, 50, 50));
	GreenBulletHit.addFrame(sf::IntRect(50, 0, 50, 50));
	GreenBulletHit.addFrame(sf::IntRect(100, 0, 50, 50));
	GreenBulletHit.addFrame(sf::IntRect(150, 0, 50, 50));
	GreenBulletHit.addFrame(sf::IntRect(200, 0, 50, 50));
	GreenBulletHit.addFrame(sf::IntRect(250, 0, 50, 50));
	GreenBulletHit.addFrame(sf::IntRect(300, 0, 50, 50));
	GreenBulletHit.addFrame(sf::IntRect(350, 0, 50, 50));
	GreenBulletHit.addFrame(sf::IntRect(400, 0, 50, 50));
	GreenBulletHit.addFrame(sf::IntRect(450, 0, 50, 50));
	GreenBulletHit.addFrame(sf::IntRect(500, 0, 50, 50));
	Animation* currentGreenBulletHitAnimation = &GreenBulletHit;
	AnimatedSprite GreenBulletHitAnimation(sf::seconds(0.1), true, false);
	GreenBulletHitAnimation.setScale(2, 2);
	GreenBulletHitAnimation.setOrigin(25, 25);
	bool SetStartGreenBulletHit;
	SetStartGreenBulletHit = false;

	sf::Texture pinkBulletHitT;
	pinkBulletHitT.loadFromFile("../textures/pink-projectile-blow-up.png");

	Animation pinkBulletHit;
	pinkBulletHit.setSpriteSheet(pinkBulletHitT);
	pinkBulletHit.addFrame(sf::IntRect(0, 0, 50, 50));
	pinkBulletHit.addFrame(sf::IntRect(50, 0, 50, 50));
	pinkBulletHit.addFrame(sf::IntRect(100, 0, 50, 50));
	pinkBulletHit.addFrame(sf::IntRect(150, 0, 50, 50));
	pinkBulletHit.addFrame(sf::IntRect(200, 0, 50, 50));
	pinkBulletHit.addFrame(sf::IntRect(250, 0, 50, 50));
	pinkBulletHit.addFrame(sf::IntRect(300, 0, 50, 50));
	pinkBulletHit.addFrame(sf::IntRect(350, 0, 50, 50));
	pinkBulletHit.addFrame(sf::IntRect(400, 0, 50, 50));
	pinkBulletHit.addFrame(sf::IntRect(450, 0, 50, 50));
	pinkBulletHit.addFrame(sf::IntRect(500, 0, 50, 50));
	Animation* currentpinkBulletHitAnimation = &pinkBulletHit;
	AnimatedSprite pinkBulletHitAnimation(sf::seconds(0.1), true, false);
	pinkBulletHitAnimation.setScale(2, 2);
	pinkBulletHitAnimation.setOrigin(25, 25);
	bool SetStartPinkBulletHit;
	SetStartPinkBulletHit = false;

	sf::Texture GinnyDeathTexture;
	GinnyDeathTexture.loadFromFile("../textures/ginny_death.png");

	Animation GinnyDeath;
	GinnyDeath.setSpriteSheet(GinnyDeathTexture);
	GinnyDeath.addFrame(sf::IntRect(0, 0, 200, 200));
	GinnyDeath.addFrame(sf::IntRect(200, 0, 200, 200));
	GinnyDeath.addFrame(sf::IntRect(400, 0, 200, 200));
	GinnyDeath.addFrame(sf::IntRect(600, 0, 200, 200));
	GinnyDeath.addFrame(sf::IntRect(800, 0, 200, 200));
	GinnyDeath.addFrame(sf::IntRect(1000, 0, 200, 200));
	GinnyDeath.addFrame(sf::IntRect(1200, 0, 200, 200));
	GinnyDeath.addFrame(sf::IntRect(1400, 0, 200, 200));
	GinnyDeath.addFrame(sf::IntRect(1600, 0, 200, 200));
	GinnyDeath.addFrame(sf::IntRect(1800, 0, 200, 200));
	GinnyDeath.addFrame(sf::IntRect(2000, 0, 200, 200));
	GinnyDeath.addFrame(sf::IntRect(2200, 0, 200, 200));
	GinnyDeath.addFrame(sf::IntRect(2400, 0, 200, 200));
	GinnyDeath.addFrame(sf::IntRect(0, 0, 0, 0));
	GinnyDeath.addFrame(sf::IntRect(0, 0, 0, 0));

	Animation* currentGinnyDeathAnimation = &GinnyDeath;
	AnimatedSprite GinnyDeathAnimation(sf::seconds(0.1), true, false);

	bool GinnyDeathAnimationPlaying = false;
	int health = 3;

	sf::RectangleShape healthRect;
	healthRect.setSize(sf::Vector2f(225, 20));
	healthRect.setFillColor(sf::Color::Green);
	healthRect.setPosition(sf::Vector2f(240, 75));

	int tylerHealth = 4;
	sf::RectangleShape tHealthRect;
	tHealthRect.setSize(sf::Vector2f(180, 15));
	tHealthRect.setFillColor(sf::Color::Green);
	tHealthRect.setPosition(300, 200);

	Animation* currentGinnyAnimation = &ginnyIdle;
	AnimatedSprite animatedSpriteGinnyIdle(sf::seconds(0.1), true, false);
	animatedSpriteGinnyIdle.setPosition(sf::Vector2f(window.getSize().x / 2,window.getSize().y/2));
	sf::RectangleShape tbulletBox(sf::Vector2f(20, 20));
	sf::RectangleShape Tyler(sf::Vector2f(80, 80));
	bool rainbowPowerBool = false;
	player.setFillColor(sf::Color::Yellow);
	bool shot = false;
	bool tshot = false;
	int bulletTimer = 0;
	sf::CircleShape megabullet(500.0f, 100);
	crosshair.setFillColor(sf::Color::Cyan);
	if (!window.isOpen())return-1;
	const float targetTime = 1.0f / 60.f;
	float accumalotor = 0.0f;
	float frameTime = 0.0f;
	sf::Keyboard keys;
	Bullet bullet(bulletPBox,bulletSprite, window);
	TylerBullets tbullet(tbulletBox, window, tbulletSprite);
	sf::Clock clock;
	sf::Clock invincibilityClock;
	sf::Clock tInvincibilityClock;
	sf::Clock intvalShotClock;
	sf::Clock intvalShotClock2;
	megabullet.setFillColor(sf::Color::Red);
	tylerHud.setPosition(300, 150);
	powerUp.setScale(0.3, 0.3);
	powerUp.setPosition(540, 60);
	hudShield.setPosition(540, 60);
	Tyler.setPosition(playerPos.x - 80, playerPos.y);
	bool rainbowActive = 0;
	bool powerup_isAlive = 0;
	rainbow.setPosition(-200, -200);

	std::ifstream AstFile("../Level/Asteroid.txt");
	if (AstFile.is_open())
	{
		int x;
		int y;
		int index;
		AstFile >> x;
		AstFile >> y;
		for (int i = 0; i < y; i++)
		{
			for (int j = 0; j < x; j++)
			{
				AstFile >> index;


				if (index == 1)
				{
					X_pos = j * 260;

					Y_pos = i * 260;
					AsterSprt1.setTexture(AsterTex1,(260,260));

					Asteroid* ast = new Asteroid(AsterSprt1 ,sf::Vector2f(X_pos, Y_pos));
					AsterArray.push_back(ast);
				}
				if (index == 2)
				{

					X_pos = j * 650;
					Y_pos = i * 390;
					AsterSprt1.setTexture(AsterTex2,(650,390));
					Asteroid* ast = new Asteroid(AsterSprt1, sf::Vector2f(X_pos, Y_pos));
					AsterArray.push_back(ast);
				}
				if (index == 3)
				{
					X_pos = j * 390;
					Y_pos = i * 390;
					AsterSprt1.setTexture(AsterTex3,(390,390));
					Asteroid* ast = new Asteroid(AsterSprt1, sf::Vector2f(X_pos, Y_pos));
					AsterArray.push_back(ast);
				}
			}
		}

	}
	AstFile.close();

	std::ifstream myfile("../Level/Lvl1.txt");
	if (myfile.is_open())
	{
		int x;
		int y;
		int index;
		myfile >> x;
		myfile >> y;
		for (int i = 0; i < y; i++)
		{
			for (int j = 0; j < x; j++)
			{
				myfile >> index;
				if (index == 1)
				{
					X_pos = j * 100;
					Y_pos = i * 117;
					EnemyCephalopods*   cep = new EnemyCephalopods(sf::Vector2f(X_pos, Y_pos), enemyCepSprite);
					SpawnPointsCep.push_back(cep);
				}
				if (index == 2)
				{
					X_pos = j * 100;
					Y_pos = i * 117;
					EnemyVaranidaes*   var = new EnemyVaranidaes(sf::Vector2f(X_pos, Y_pos), enemyVarSprite);
					SpawnPointsVar.push_back(var);
				}
			}
		}
	}
	myfile.close();

	// Komets file .TXT
	std::ifstream newfile("../Level/Komets.txt");
	if (newfile.is_open())
	{
		int x;
		int y;
		int index;
		newfile >> x;
		newfile >> y;
		for (int i = 0; i < y; i++)
		{
			for (int j = 0; j < x; j++)
			{
				newfile >> index;
				if (index == 1)
				{
					Xc_pos = j * 160;
					Yc_pos = i * 160;
					Comet*   com = new Comet(cometsprt, sf::Vector2f(Xc_pos, Yc_pos));
					cometArray.push_back(com);
				}
			}
		}
	}
	newfile.close();

	while (window.isOpen())
	{
		sf::Time deltaTime = clock.restart();
		frameTime = std::min(deltaTime.asSeconds(), 0.1f);
		accumalotor += frameTime;
		while (accumalotor > targetTime)
		{
			accumalotor -= targetTime;
			sf::Event event;
			while (window.pollEvent(event))
			{


				if (event.type == sf::Event::Closed)
				{
					window.close();
				}
			}
		}
		if (gameEstate == MENUSTATE)
		{
			if (keys.isKeyPressed(sf::Keyboard::W) && keypressed0 == false)
			{
				menu.MoveUP();
				soundManager->PlayEffect("hover");
				keypressed0 = true;
			}
			if (!sf::Keyboard::isKeyPressed(sf::Keyboard::W) && keypressed0 == true)
			{
				keypressed0 = false;
			}
			if (keys.isKeyPressed(sf::Keyboard::S) && keypressed1 == false)
			{
				menu.MoveDown();
				soundManager->PlayEffect("hover");
				keypressed1 = true;
			}
			if (!sf::Keyboard::isKeyPressed(sf::Keyboard::S) && keypressed1 == true)
			{
				keypressed1 = false;
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space) && keyreturn == false)
			{
				//soundManager->PlayEffect("click");
				keyreturn = true;
				switch (menu.GetPressedItem())
				{
					
				case 0:
				{
					gameEstate = GAMESTATE;
					//	std::cout << "play" << std::endl;
					soundManager->PlayMusic("../sound/music/stuff.ogg");
					soundManager->PlayEffect("jatpack");
					window.setMouseCursorVisible(false);
					break;
				}
				case 1:
				{
					gameEstate = HIGHSCORESTATE;
				
						break;
				}
				case 2:
				{

					window.close();
					break;
				}
				}
			}
			if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Space) && keyreturn == true)
			{
				keyreturn = false;
			}
			window.draw(menuSprite);
			menu.draw(window);
		}
		else if (gameEstate == HIGHSCORESTATE)
		{

			window.setMouseCursorVisible(false);
			
			std::ifstream streamin;
			streamin.open("../Level/scorein.txt");

			if (streamin.is_open())
			{
				std::string line[8];
				streamin >> line[0];
				streamin >> line[1];
				streamin >> line[2];
				streamin >> line[3];
				streamin >> line[4];
				streamin >> line[5];
				streamin >> line[6];
				streamin >> line[7];
				
					if (line[0].empty())
					{
						break;
					}
				
					sf::String ScoreName;
					sf::Text Name;
					Name.setFont(font);
					Name.setString(ScoreName);

					sf::Text Highscore;
					Highscore.setFont(font);
					Highscore.setCharacterSize(45);
					Highscore.setString("1ST:" + line[0]);
					Highscore.setPosition(500, 450);

					sf::Text SecondScore;
					SecondScore.setFont(font);
					SecondScore.setCharacterSize(45);
					SecondScore.setString("2ND:" + line[1]);
					SecondScore.setPosition(500, 550);

					sf::Text ThirdScore;
					ThirdScore.setFont(font);
					ThirdScore.setCharacterSize(45);
					ThirdScore.setString("3RD:" + line[2]);
					ThirdScore.setPosition(500, 650);

					sf::Text ForthScore;
					ForthScore.setFont(font);
					ForthScore.setCharacterSize(45);
					ForthScore.setString("4TH:" + line[3]);
					ForthScore.setPosition(500, 750);

					sf::Text FifthScore;
					FifthScore.setFont(font);
					FifthScore.setCharacterSize(45);
					FifthScore.setString("5TH:" + line[4]);
					FifthScore.setPosition(1100, 450);

					sf::Text SixthScore;
					SixthScore.setFont(font);
					SixthScore.setCharacterSize(45);
					SixthScore.setString("6TH:" + line[5]);
					SixthScore.setPosition(1100, 550);

					sf::Text SeventhScore;
					SeventhScore.setFont(font);
					SeventhScore.setCharacterSize(45);
					SeventhScore.setString("7TH:" + line[6]);
					SeventhScore.setPosition(1100, 650);

					sf::Text EighthScore;
					EighthScore.setFont(font);
					EighthScore.setCharacterSize(45);
					EighthScore.setString("8TH:" + line[7]);
					EighthScore.setPosition(1100, 750);
				
				if (keys.isKeyPressed(sf::Keyboard().Escape))
				{

					gameEstate = MENUSTATE;
				}

				streamin.close();
					window.draw(HallOfFameSPRT);
					window.draw(Highscore);
					window.draw(SecondScore);
					window.draw(ThirdScore);
					window.draw(ForthScore);
					window.draw(FifthScore);
					window.draw(SixthScore);
					window.draw(SeventhScore);
					window.draw(EighthScore);
				
					}
			
			

			

		}
		
		else if (gameEstate==GAMESTATE)
		{

			// not working how we want it.
			if (keys.isKeyPressed(sf::Keyboard::P))
			{
				
				if (pause == false)
				{
					soundManager->PlayEffect("pause");
					
					pause = true;

				}
				window.setMouseCursorVisible(true);
			}
			if (keys.isKeyPressed(sf::Keyboard::O))
			{

			if (pause == true)
			{
				pause = false;
			}
			window.setMouseCursorVisible(false);
			}
			if (pause == true)
				window.draw(PauseSprite);

			if (pause == false)
			{
				if (mp > 1)
				{
					if (mpTtl > 0)
					{
						mpTtl -= 1;
					}
					if (mpTtl <= 0)
					{
						//	mp -= 0.5;
						//	mpTtl = 50;
						mp = 1;
					}
				}
				sf::Time invincibilityTime = invincibilityClock.getElapsedTime();
				sf::Time intevalshotTime = intvalShotClock.getElapsedTime();
				sf::Time intevalshotTime2 = intvalShotClock2.getElapsedTime();
				sf::Time tInvincibilityTime = tInvincibilityClock.getElapsedTime();
				sf::Vector2f mousePos = window.mapPixelToCoords(sf::Mouse::getPosition(window));
				CrossHair.setPosition(mousePos.x, mousePos.y);
				player.setPosition(playerPos);
				if (keys.isKeyPressed(sf::Keyboard().W) && playerPos.y >= 10)
				{
					playerPos.y -= 10;
					tutorialMoveActiveW = false;
				}
				if (keys.isKeyPressed(sf::Keyboard().S) && playerPos.y <= 1000)
				{
					playerPos.y += 10;
					tutorialMoveActiveS = false;
				}
				if (keys.isKeyPressed(sf::Keyboard().A) && playerPos.x >= -25)
				{
					playerPos.x -= 10;
					tutorialMoveActiveA = false;
				}
				if (keys.isKeyPressed(sf::Keyboard().D) && playerPos.x <= 1810)
				{
					playerPos.x += 10;
					tutorialMoveActiveD = false;
				}
				sf::Vector2f distance(playerPos.x - Tyler.getPosition().x, playerPos.y - Tyler.getPosition().y);
				if (distance.x > 150 && tylerLives == true)
				{
					Tyler.move(6, 0);
				}
				if (distance.x < 25 && tylerLives == true)
				{
					Tyler.move(-6, 0);
				}
				if (distance.y > 125 && tylerLives == true)
				{
					Tyler.move(0, 6);
				}
				if (distance.y < 25 && tylerLives == true)
				{
					Tyler.move(0, -6);
				}
				//if (tylerLives == false)
				//{
				//	Tyler.setPosition(-200, 200);
				//}
				window.clear(sf::Color(0x11, 0x12, 0x33, 0xff));
				// bg + parlax
				window.draw(backgroundSprite);
				window.draw(backgroundSprite2);
				window.draw(BG_StarsSprite1);
				window.draw(BG_StarsSprite2);
				window.draw(BG_StarsSprite3);
				window.draw(BG_StarsSprite1copy);
				window.draw(BG_StarsSprite2copy);
				window.draw(BG_StarsSprite3copy);

				backgroundSprite.move(-0.3, 0);
				backgroundSprite2.move(-0.3, 0);
				BG_StarsSprite1.move(-0.5, 0);
				BG_StarsSprite2.move(-0.8, 0);
				BG_StarsSprite3.move(-1, 0);
				BG_StarsSprite1copy.move(-0.5, 0);
				BG_StarsSprite2copy.move(-0.8, 0);
				BG_StarsSprite3copy.move(-1, 0);
				if (backgroundSprite.getPosition().x <= -1920)
				{
					backgroundSprite.setPosition(1920, 0);
				}
				if (backgroundSprite2.getPosition().x <= -1920)
				{
					backgroundSprite2.setPosition(1920, 0);
				}
				if (BG_StarsSprite1.getPosition().x <= -1920)
				{
					BG_StarsSprite1.setPosition(1920, 0);
				}
				if (BG_StarsSprite2.getPosition().x <= -1920)
				{
					BG_StarsSprite2.setPosition(1920, 0);
				}
				if (BG_StarsSprite3.getPosition().x <= -1920)
				{
					BG_StarsSprite3.setPosition(1920, 0);
				}
				if (BG_StarsSprite1copy.getPosition().x <= -1920)
				{
					BG_StarsSprite1copy.setPosition(1920, 0);
				}
				if (BG_StarsSprite2copy.getPosition().x <= -1920)
				{
					BG_StarsSprite2copy.setPosition(1920, 0);
				}
				if (BG_StarsSprite3copy.getPosition().x <= -1920)
				{
					BG_StarsSprite3copy.setPosition(1920, 0);
				}


				


				

				ScoreText.setString("Score:" + std::to_string(Score));
				//mpText.setString("x " + std::to_string(mp));

				ScoreText.setPosition(950, 0);
				//mpText.setPosition(950, 30);

				animatedSpriteGinnyIdle.play(*currentGinnyAnimation);
				if (tylerLives == true)
				{
					animatedTylerSpriteIdle.play(*currentTylerAnimation);
					animatedTylerSpriteIdle.update(deltaTime);
				}
				if (tylerLives == false)
				{
					if (TylerDeathAnimationPlaying == false)
					{
						animatedTylerDeathSprite.play(*currentTylerDeathAnimation);
						TylerDeathAnimationPlaying = true;
						animatedTylerDeathSprite.setPosition(animatedTylerSpriteIdle.getPosition().x -50, animatedTylerSpriteIdle.getPosition().y-50);
					}
					if (animatedTylerDeathSprite.getFrame() != 13)
					{
						tutorialRightActive = false;
						animatedTylerDeathSprite.update(deltaTime);
					}
				
				}
				animatedSpriteGinnyIdle.update(deltaTime);
				animatedRainbowSpritePowerUp.play(*currentRainbowPowerUpAnimation);
				animatedRainbowSpritePowerUp.update(deltaTime);
			
				if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && shot == false && intevalshotTime.asSeconds() > 0.5f)
				{
					soundManager->PlayEffect("plop");
					bullet.SetAngle(float(atan2((mousePos.y - playerPos.y), (mousePos.x - playerPos.x))));
					bullet.rect.setPosition(ginnyHead.getPosition());
					bullet.mousePos2 = window.mapPixelToCoords(sf::Mouse::getPosition(window));
					float bdx = ginnyHead.getPosition().x - sf::Mouse::getPosition().x;
					float bdy = ginnyHead.getPosition().y - sf::Mouse::getPosition().y;
					float bRotation = ((atan2(bdy, bdx)) * 180 / 3.14159265359) + 180;
					bullet.SetBulletRotation(bRotation);
					
					bulletArray.push_back(bullet);
					shot = true;
					intvalShotClock.restart();

					tutorialLeftActive = false;
				}
				if (!sf::Mouse::isButtonPressed(sf::Mouse::Left) && shot == true)
				{
					shot = false;
				}
				if (tylerLives == true)
				{
					if (sf::Mouse::isButtonPressed(sf::Mouse::Right) && tshot == false && intevalshotTime2.asSeconds() > 1.0f)
					{
						soundManager->PlayEffect("Tylerplop");
						tbullet.SetAngle(float(atan2((mousePos.y - Tyler.getPosition().y), (mousePos.x - Tyler.getPosition().x))));
						tbullet.rect.setPosition(Tyler.getPosition());
						tbullet.mousePos2 = window.mapPixelToCoords(sf::Mouse::getPosition(window));
						tbulletArray.push_back(tbullet);
						tshot = true;
						intvalShotClock2.restart();
						tutorialRightActive = false;
					}
					if (!sf::Mouse::isButtonPressed(sf::Mouse::Right) && tshot == true)
					{
						tshot = false;
					}
				}
				animatedSpritePowerUp.play(*currentPowerUpAnimation);
				animatedSpritePowerUp.update(deltaTime);
				for (unsigned int i = 0; i < SpawnPointsCep.size(); i++)
				{
					if (!SpawnPointsCep.empty() &&
						SpawnPointsCep.at(i)->IsAlive == true)
					{
						SpawnPointsCep.at(i)->Update(targetTime);
					}
					if (SpawnPointsCep.at(i)->GetPosition().x < -200)
						SpawnPointsCep.erase(SpawnPointsCep.begin() + i);
				}
				for (unsigned int i = 0; i < SpawnPointsVar.size(); i++)
				{
					if (!SpawnPointsVar.empty() &&
						SpawnPointsVar.at(i)->IsAlive == true)
					{
						SpawnPointsVar.at(i)->Update(targetTime);
					}

					if (SpawnPointsVar.at(i)->GetPosition().x < -200)
						SpawnPointsVar.erase(SpawnPointsVar.begin() + i);
				}
				for (unsigned int i = 0; i < AsterArray.size(); i++)
				{

					if (!AsterArray.empty())
					{
						AsterArray.at(i)->Update(targetTime);
					}
				
				


				}
				for (unsigned int i = 0; i < cometArray.size(); i++)
				{
					if (!cometArray.empty() &&
						cometArray.at(i)->IsAlive == true)
					{
						cometArray.at(i)->Update(targetTime);
					}
					if (cometArray.at(i)->IsAlive == false)
					{
						cometArray.at(i)->Update(targetTime);
						AnimatedCometExplosion.update(deltaTime);
						if (!AnimatedCometExplosion.isPlaying())
						{
							AnimatedCometExplosion.setFrame(12);
							AnimatedCometExplosion.setFrameTime(sf::seconds(0.1));
							AnimatedCometExplosion.pause();
						}
					}
					if (cometArray.at(i)->GetPosition().x < -200)
						cometArray.erase(cometArray.begin() + i);
				}
				bool FoundAlive = false;
				for (unsigned int i = 0; i < SpawnPointsCep.size(); i++)
				{
					if (SpawnPointsCep.at(i)->IsAlive == true)
					{
						FoundAlive = true;
					}

				}
				bool FoundAlive2 = false;
				for (unsigned int i = 0; i < SpawnPointsVar.size(); i++)
				{
					if (SpawnPointsVar.at(i)->IsAlive == true)
					{
						FoundAlive2 = true;
					}
				}
				if (FoundAlive == false && FoundAlive2 == false)
				{
					gameEstate = WINSTATE;
					soundManager->PlayMusic("../sound/music/menustuffv2.ogg");
				}
				soundManager->update();
				//KOLLITION MED SPELARE
				for (int itcometcollitionplayer = 0; itcometcollitionplayer < cometArray.size(); itcometcollitionplayer++)
				{

					if (cometArray.at(itcometcollitionplayer)->IsAlive == true)
					{
						if (playerRect.getGlobalBounds().intersects(cometArray.at(itcometcollitionplayer)->GetRect()) && invincibilityTime.asSeconds() > 3.0f && shield == false)
						{
							soundManager->PlayEffect("ginnyTakingDamage");
							soundManager->PlayEffect("GttingHit");

							Score -= 25;
							AnimatedCometExplosion.play(*currentCometExpl);
							AnimatedCometExplosion.setPosition(cometArray.at(itcometcollitionplayer)->GetPosition());
							cometArray.at(itcometcollitionplayer)->IsAlive = false;
							invincibilityClock.restart();
							health--;
							crateHitText(-25, cometArray.at(itcometcollitionplayer)->GetPosition()); //sf::Vector2f(500,500)
							mpTtl = 0;
							mp = 1;

						}
					}

				}
				for (int itastercollisionplayer = 0; itastercollisionplayer < AsterArray.size(); itastercollisionplayer++)
				{
					sf::FloatRect area;
					if (playerRect.getGlobalBounds().intersects(AsterArray.at(itastercollisionplayer)->GetRect(), area))
					{
						
					
							// Verifying if we need to apply collision to the vertical axis, else we apply to horizontal axis
							if (area.width > area.height)
							{
								if (area.contains({ area.left, playerRect.getPosition().y }))
								{
									// Up side crash
									/*playerRect.setPosition({ playerRect.getPosition().x, playerRect.getPosition().y + area.height });*/
									playerPos.y += 10;

								}
								else
								{
									// Down side crash
								//	playerRect.setPosition({ playerRect.getPosition().x, playerRect.getPosition().y - area.height });
									playerPos.y -= 10;
								}
							}
							else if (area.width < area.height)
							{
								if (area.contains({ playerRect.getPosition().x + playerRect.getGlobalBounds().width - 1.f, area.top + 1.f }))
								{
									//Right side crash
								//	playerRect.setPosition({ playerRect.getPosition().x - area.width, playerRect.getPosition().y });
									playerPos.x -= 11;
									if (area.contains({ playerRect.getPosition().x + playerRect.getGlobalBounds().width - 1.f, area.top + 1.f }) && playerPos.x <= -26)
									{
										health = 0;
									}
								}
								else
								{
									//Left side crash
									/*playerRect.setPosition({ playerRect.getPosition().x + area.width, playerRect.getPosition().y });*/
									playerPos.x += 10;
								}
							}
						
					}

				}
				

				auto iterAsteroid = AsterArray.begin();
				while (iterAsteroid != AsterArray.end())
				{
					window.draw((*iterAsteroid)->GetSprite());

					iterAsteroid++;
				}

				int counter = 0;
				for (unsigned int i = 0; i < bulletArray.size(); i++)
				{

					if (bulletArray[counter].GetIsAlive() == true)
					{

						bulletArray[counter].Update(targetTime, window, playerPos);
						window.draw(bulletArray[counter].GetSprite());//GetSprite());

					}
					

				
					
					for (int itBullets = 0; itBullets < SpawnPointsCep.size(); itBullets++)
					{
						if (SpawnPointsCep.at(itBullets)->IsAlive == true)
						{

							if (SpawnPointsCep.at(itBullets)->GetRect().intersects(bulletArray[counter].GetRect()))
							{
								if (bulletArray[counter].GetIsAlive() == true)
								{

									soundManager->PlayEffect("eDeath");
									SpawnPointsCep.at(itBullets)->IsAlive = false;
									bulletArray[counter].SetIsAlive(false);

									crateHitText(100 * mp, SpawnPointsCep.at(itBullets)->GetPosition()); //sf::Vector2f(500,500)
									Score += 100 * mp;
									mpTtl = 120;
									mp += 0.5;
								}
							}
						}
					}
					
					for (int itBullets = 0; itBullets < SpawnPointsVar.size(); itBullets++)
					{
						if (SpawnPointsVar.at(itBullets)->IsAlive == true)
						{
							if (SpawnPointsVar.at(itBullets)->GetRect().intersects(bulletArray[counter].GetRect()))
							{
								if (bulletArray[counter].GetIsAlive() == true)
								{
									soundManager->PlayEffect("eDeath");

									bulletArray[counter].SetIsAlive(false);
									if (SpawnPointsVar.at(itBullets)->GetHealth() == 1)
									{
										crateHitText(200 * mp, SpawnPointsVar.at(itBullets)->GetPosition());
										Score += 200 * mp;
										mpTtl = 120;
										mp += 0.5;
									}
									SpawnPointsVar.at(itBullets)->SetHealthLeft(SpawnPointsVar.at(itBullets)->GetHealth() - 1);
									if (SpawnPointsVar.at(itBullets)->GetHealth() == 1)
									{
									}
								}
							}
						}
					}

					//for (int itbulletcollcage = 0; itbulletcollcage < bulletArray.size(); itbulletcollcage++)
					//{
					//
					//	if (cagerect.getGlobalBounds().intersects(bulletArray.at(itbulletcollcage)));
					//	{

					//		cage->Death();
					//		bulletArray[counter].SetIsAlive(false);
					//	}

					//}



					for (int itBullets = 0; itBullets < cometArray.size(); itBullets++)
					{
						if (cometArray.at(itBullets)->IsAlive == true)
						{
							if (bulletArray[counter].GetIsAlive() == true)
							{
								if (cometArray.at(itBullets)->GetRect().intersects(bulletArray[counter].GetRect()))
								{
									AnimatedCometExplosion.play(*currentCometExpl);
									soundManager->PlayEffect("kometExplosion");

									AnimatedCometExplosion.setPosition(cometArray.at(itBullets)->GetPosition());
									cometArray.at(itBullets)->IsAlive = false;
									bulletArray[counter].SetIsAlive(false);

									crateHitText(50 * mp, cometArray.at(itBullets)->GetPosition()); //sf::Vector2f(500,500)
									Score += 50 * mp;
									mpTtl = 120;
									mp += 0.5;

									int random = std::rand() % 5;
									if (random == 0)
									{
										if (animatedRainbowSpritePowerUp.getPosition().x == -200 && animatedRainbowSpritePowerUp.getPosition().y == -200)
										{

											animatedRainbowSpritePowerUp.setPosition(cometArray.at(itBullets)->GetPosition());


											powerup_isAlive = true;

										}
									}
									else if (random == 1)
									{
										if (animatedShieldSpritePowerUp.getPosition().x == -200 && animatedShieldSpritePowerUp.getPosition().y == -200)
										{
											animatedShieldSpritePowerUp.setPosition(cometArray.at(itBullets)->GetPosition());

											powerup_isAlive = true;
										}
									}
									else if (random == 2)
									{
										//somting
									}
									else
									{
										//Nothing happens
									}

								}
							}
						}
					}
					
					

					if (bulletArray[counter].rect.getPosition().x < 0 ||
						bulletArray[counter].rect.getPosition().y < 0 ||
						bulletArray[counter].rect.getPosition().x > 1920 ||
						bulletArray[counter].rect.getPosition().y > 1080)
					{
						bulletArray.erase(bulletArray.begin() + i);
					}
					counter++;
				}
				int tcounter = 0;
				for (unsigned int i = 0; i < tbulletArray.size(); i++)
				{

					if (tbulletArray[tcounter].GetIsAlive() == true)
					{

						tbulletArray[tcounter].Update(targetTime, window, Tyler.getPosition());
						window.draw(tbulletArray[tcounter].GetSprite());//GetSprite());
					}

					for (int ittBullets = 0; ittBullets < SpawnPointsCep.size(); ittBullets++)
					{
						if (SpawnPointsCep.at(ittBullets)->IsAlive == true)
						{
							if (SpawnPointsCep.at(ittBullets)->GetRect().intersects(tbulletArray[tcounter].GetRect()))
							{
								if (tbulletArray[tcounter].GetIsAlive() == true)
								{
									soundManager->PlayEffect("eDeath");
									Score += 200 * mp;
									SpawnPointsCep.at(ittBullets)->IsAlive = false;
									tbulletArray[tcounter].SetIsAlive(false);
									crateHitText(200 * mp, SpawnPointsCep.at(ittBullets)->GetPosition());
									mpTtl = 120;
									mp += 0.5;
								}
							}
						}
					}

					for (int ittBullets = 0; ittBullets < SpawnPointsVar.size(); ittBullets++)
					{
						if (SpawnPointsVar.at(ittBullets)->IsAlive == true)
						{
							if (SpawnPointsVar.at(ittBullets)->GetRect().intersects(tbulletArray[tcounter].GetRect()))
							{
								if (tbulletArray[tcounter].GetIsAlive() == true)
								{
									soundManager->PlayEffect("eDeath");
									Score += 100 * mp;
									tbulletArray[tcounter].SetIsAlive(false);
									crateHitText(100 * mp, SpawnPointsVar.at(ittBullets)->GetPosition());
									mpTtl = 120;
									mp += 0.5;
									SpawnPointsVar.at(ittBullets)->SetHealthLeft(SpawnPointsVar.at(ittBullets)->GetHealth() - 2);
								}
							}
						}
					}
					


					for (int ittBullets = 0; ittBullets < cometArray.size(); ittBullets++)
					{
						if (cometArray.at(ittBullets)->IsAlive == true)
						{
							if (tbulletArray[tcounter].GetIsAlive() == true)
							{
								if (cometArray.at(ittBullets)->GetRect().intersects(tbulletArray[tcounter].GetRect()))
								{
									Score += 50 * mp;
									soundManager->PlayEffect("kometExplosion");
									AnimatedCometExplosion.play(*currentCometExpl);

									AnimatedCometExplosion.setPosition(cometArray.at(ittBullets)->GetPosition());
									cometArray.at(ittBullets)->IsAlive = false;
									tbulletArray[tcounter].SetIsAlive(false);

									crateHitText(50 * mp, cometArray.at(ittBullets)->GetPosition());
									mpTtl = 120;
									mp += 0.5;

									int random = std::rand() % 5;
									if (random == 0)
									{
										if (animatedRainbowSpritePowerUp.getPosition().x == -200 && animatedRainbowSpritePowerUp.getPosition().y == -200)
										{
											animatedRainbowSpritePowerUp.setPosition(cometArray.at(ittBullets)->GetPosition());
											powerup_isAlive = true;
										}
									}
									else if (random == 1)
									{
										if (animatedShieldSpritePowerUp.getPosition().x == -200 && animatedShieldSpritePowerUp.getPosition().y == -200)
										{
											animatedShieldSpritePowerUp.setPosition(cometArray.at(ittBullets)->GetPosition());

											powerup_isAlive = true;
										}
									}
									else
									{
										//somting
									}
								}
							}
						}
					}
					if (tbulletArray[tcounter].rect.getPosition().x < 0 ||
						tbulletArray[tcounter].rect.getPosition().y < 0 ||
						tbulletArray[tcounter].rect.getPosition().x > 1920 ||
						tbulletArray[tcounter].rect.getPosition().y > 1080)
					{
						tbulletArray.erase(tbulletArray.begin() + i);
					}
					tcounter++;
				}
				for (int itEnemyCep = 0; itEnemyCep < SpawnPointsCep.size(); itEnemyCep++)
				{
					if (SpawnPointsCep.at(itEnemyCep)->GetPosition().x <= 1920)
					{
						if (SpawnPointsCep.at(itEnemyCep)->GetFireAray() == true)
						{
							if (SpawnPointsCep.at(itEnemyCep)->GetIsAlive() == true)
							{
								if (SpawnPointsCep.at(itEnemyCep)->GetFireAngle() == true)
								{
									enemybullet.SetEnemyIsVar(false);
									enemybullet.rect.setPosition(SpawnPointsCep.at(itEnemyCep)->GetSprite().getPosition());
									enemybullet.rect.setPosition(enemybullet.rect.getPosition().x, enemybullet.rect.getPosition().y + 60);
									enemybullet.SetAnagle(180, 220);
									enemybulletArray.push_back(enemybullet);
								}
								else
								{
									enemybullet.SetEnemyIsVar(false);
									enemybullet.rect.setPosition(SpawnPointsCep.at(itEnemyCep)->GetSprite().getPosition());
									enemybullet.rect.setPosition(enemybullet.rect.getPosition().x, enemybullet.rect.getPosition().y + 100);
									enemybullet.SetAnagle(180, 30);
									enemybulletArray.push_back(enemybullet);
								}
							}
							SpawnPointsCep.at(itEnemyCep)->SetFireAray(0);
						}
					}
				}
			

				for (int itEnemyVar = 0; itEnemyVar < SpawnPointsVar.size(); itEnemyVar++)
				{
					if (SpawnPointsVar.at(itEnemyVar)->GetPosition().x <= 1920)
					{
						if (SpawnPointsVar.at(itEnemyVar)->GetFireAray() == true)
						{
							if (SpawnPointsVar.at(itEnemyVar)->GetIsAlive() == true)
							{
								enemybullet.SetEnemyIsVar(true);
								enemybullet.rect.setPosition(SpawnPointsVar.at(itEnemyVar)->GetSprite().getPosition());
								enemybullet.rect.setPosition(enemybullet.rect.getPosition().x, enemybullet.rect.getPosition().y + 55);
								enemybullet.SetAnagle(180, 180);
								enemybulletArray.push_back(enemybullet);
							}
							SpawnPointsVar.at(itEnemyVar)->SetFireAray(0);
						}
					}
				}

				GreenBulletHitAnimation.update(deltaTime);
				window.draw(GreenBulletHitAnimation);
				if (GreenBulletHitAnimation.getFrame() == 10)
				{
					GreenBulletHitAnimation.setFrame(10, false);
					GreenBulletHitAnimation.setPosition(-200, -200);
					GreenBulletHitAnimation.pause();
				}
				if (GreenBulletHitAnimation.getPosition().x == -200)
				{
					GreenBulletHitAnimation.stop();
					GreenBulletHitAnimation.setFrameTime(sf::seconds(0.1));
					SetStartGreenBulletHit = false;
				}

				pinkBulletHitAnimation.update(deltaTime);
				window.draw(pinkBulletHitAnimation);
				if (pinkBulletHitAnimation.getFrame() == 10)
				{
					pinkBulletHitAnimation.setFrame(10, false);
					pinkBulletHitAnimation.setPosition(-200, -200);
					pinkBulletHitAnimation.pause();
				}
				if (pinkBulletHitAnimation.getPosition().x == -200)
				{
					pinkBulletHitAnimation.stop();
					pinkBulletHitAnimation.setFrameTime(sf::seconds(0.1));
					SetStartPinkBulletHit = false;
				}

				int e_counter = 0;
				for (unsigned int i = 0; i < enemybulletArray.size(); i++)
				{
					if (enemybulletArray[e_counter].GetIsAlive() == true)

					{
						if (tylerLives == true)
						{
							if (enemybulletArray[e_counter].GetRect().intersects(animatedTylerSpriteIdle.getGlobalBounds()) && tInvincibilityTime.asSeconds() > 3.0f && shield == false)
							{

								soundManager->PlayEffect("tylerTakingDamage");
								soundManager->PlayEffect("GttingHit");
								tInvincibilityClock.restart();
								tylerHealth --;
							}

							if (enemybulletArray[e_counter].GetRect().intersects(animatedTylerSpriteIdle.getGlobalBounds()))
							{
								enemybulletArray[e_counter].SetIsAlive(false);

								if (SetStartGreenBulletHit == false && enemybulletArray[e_counter].GetEnemyIsVar() == false)
								{
									GreenBulletHitAnimation.setPosition(enemybulletArray[e_counter].GetSprite().getPosition());
									SetStartGreenBulletHit = true;
									GreenBulletHitAnimation.play(*currentGreenBulletHitAnimation);
								}
								if (SetStartPinkBulletHit == false && enemybulletArray[e_counter].GetEnemyIsVar() == true)
								{
									pinkBulletHitAnimation.setPosition(enemybulletArray[e_counter].GetSprite().getPosition());
									SetStartGreenBulletHit = true;
									pinkBulletHitAnimation.play(*currentpinkBulletHitAnimation);
								}
							}
						}

						if (enemybulletArray[e_counter].GetRect().intersects(playerRect.getGlobalBounds()) && invincibilityTime.asSeconds() > 3.0f && shield == false)
						{
							if (SetStartGreenBulletHit == false && enemybulletArray[e_counter].GetEnemyIsVar() == false)
							{
								GreenBulletHitAnimation.setPosition(enemybulletArray[e_counter].GetSprite().getPosition());
								SetStartGreenBulletHit = true;
								GreenBulletHitAnimation.play(*currentGreenBulletHitAnimation);
							}
							if (enemybulletArray[e_counter].GetEnemyIsVar() == true)
							{
								if (SetStartPinkBulletHit == false)
								{
									pinkBulletHitAnimation.setPosition(enemybulletArray[e_counter].GetSprite().getPosition());
									SetStartPinkBulletHit = true;
									pinkBulletHitAnimation.play(*currentpinkBulletHitAnimation);
								}
							}
							soundManager->PlayEffect("ginnyTakingDamage");
							soundManager->PlayEffect("GttingHit");
							invincibilityClock.restart();
							health--;
						}
						if (enemybulletArray[e_counter].GetRect().intersects(playerRect.getGlobalBounds()))
						{
							soundManager->PlayEffect("GttingHit");
							enemybulletArray[e_counter].SetIsAlive(false);
							if (SetStartGreenBulletHit == false && enemybulletArray[e_counter].GetEnemyIsVar() == false)
							{
								GreenBulletHitAnimation.setPosition(enemybulletArray[e_counter].GetSprite().getPosition());
								SetStartGreenBulletHit = true;
								GreenBulletHitAnimation.play(*currentGreenBulletHitAnimation);
							}
							if (SetStartPinkBulletHit == false && enemybulletArray[e_counter].GetEnemyIsVar() == true)
							{
								pinkBulletHitAnimation.setPosition(enemybulletArray[e_counter].GetSprite().getPosition());
								SetStartGreenBulletHit = true;
								pinkBulletHitAnimation.play(*currentpinkBulletHitAnimation);
							}
						}
					}
					if (!enemybulletArray.empty())
					{
						enemybulletArray[e_counter].Update(targetTime, window);
						if (enemybulletArray[e_counter].GetIsAlive() == true)
						{
							window.draw(enemybulletArray[e_counter].GetSprite());//GetSprite());
						}
					}
					if (enemybulletArray[e_counter].rect.getPosition().x < 32)
					{
						enemybulletArray.erase(enemybulletArray.begin() + i);
					}
					e_counter++;
				}
				if (tylerLives == true)
				{
					tylerHeadSprite.setOrigin(tylerHeadSprite.getGlobalBounds().width / 2 - 5, tylerHeadSprite.getGlobalBounds().height / 2);
					float tdx = tylerHeadSprite.getPosition().x - mousePos.x;
					float tdy = tylerHeadSprite.getPosition().y - mousePos.y;
					float trotation = ((atan2(tdy, tdx)) * 180 / pi) + 180;
					tylerHeadSprite.setRotation(trotation);
					tylerHeadSprite.setPosition(animatedTylerSpriteIdle.getPosition().x + 90, animatedTylerSpriteIdle.getPosition().y);
				}
				if (GinnyDeathAnimationPlaying == false)
				{
					
					ginnyHead.setOrigin(ginnyHead.getGlobalBounds().width / 2 - 5, ginnyHead.getGlobalBounds().height / 2);
					float dx = ginnyHead.getPosition().x - mousePos.x;
					float dy = ginnyHead.getPosition().y - mousePos.y;
					float rotation = ((atan2(dy, dx)) * 180 / pi) + 180;
				
				

					ginnyHead.setRotation(rotation);

					ginnyHead.setPosition(animatedSpriteGinnyIdle.getPosition().x + 110, animatedSpriteGinnyIdle.getPosition().y);
				}
				for (int itPlayercollitionEnemy = 0; itPlayercollitionEnemy < SpawnPointsCep.size(); itPlayercollitionEnemy++)
				{
					if (SpawnPointsCep.at(itPlayercollitionEnemy)->GetIsAlive() == true)
					{
						if (playerRect.getGlobalBounds().intersects(SpawnPointsCep.at(itPlayercollitionEnemy)->GetRect()) && invincibilityTime.asSeconds() > 3.0f && shield == false)
						{
							soundManager->PlayEffect("ginnyTakingDamage");
							soundManager->PlayEffect("GttingHit");
							invincibilityClock.restart();
							health--;
						}

						if (animatedTylerSpriteIdle.getGlobalBounds().intersects(SpawnPointsCep.at(itPlayercollitionEnemy)->GetRect()) && tInvincibilityTime.asSeconds() > 3.0f && shield == false)
						{
							if (tylerLives == true)
							{
								soundManager->PlayEffect("tylerTakingDamage");
								soundManager->PlayEffect("GttingHit");
								tInvincibilityClock.restart();
								tylerHealth--;
							}
						}
					}
				}

				for (int itPlayercollitionEnemy = 0; itPlayercollitionEnemy < SpawnPointsVar.size(); itPlayercollitionEnemy++)
				{
					if (SpawnPointsVar.at(itPlayercollitionEnemy)->GetIsAlive() == true)
					{
						if (playerRect.getGlobalBounds().intersects(SpawnPointsVar.at(itPlayercollitionEnemy)->GetRect()) && invincibilityTime.asSeconds() > 3.0f && shield == false)
						{
							soundManager->PlayEffect("ginnyTakingDamage");
							soundManager->PlayEffect("GttingHit"); 
							invincibilityClock.restart();
							health--;
						}
						if (animatedTylerSpriteIdle.getGlobalBounds().intersects(SpawnPointsVar.at(itPlayercollitionEnemy)->GetRect()) && tInvincibilityTime.asSeconds() > 3.0f && shield == false )
						{
							if (tylerLives == true)
							{
								soundManager->PlayEffect("tylerTakingDamage");
								soundManager->PlayEffect("GttingHit");
								tInvincibilityClock.restart();
								tylerHealth--;
							}
						}
					}
				}

				rainbowPowerUpRect.setPosition(animatedRainbowSpritePowerUp.getPosition());
				shieldPowerUpRect.setPosition(animatedShieldSpritePowerUp.getPosition());
				if (shield == true)
				{
					if (IsPlayinganimatedShieldSprite == false)
					{
						IsPlayinganimatedShieldSprite = true;
						animatedShieldSprite.play(*currentShieldAnimation);
						if (tylerLives == true)
						{
							TyleranimatedShieldSprite.play(*TylercurrentShieldAnimation);
						}
					}
					animatedShieldSprite.update(deltaTime);
					if (tylerLives == true)
					{
						TyleranimatedShieldSprite.update(deltaTime);
					}
				}
				else if (shield == false)
				{
					animatedShieldSprite.stop();
					TyleranimatedShieldSprite.stop();
					IsPlayinganimatedShieldSprite = false;
				}
			if (health <= 0)
			{	
				

			
				
				
				if (GinnyDeathAnimationPlaying == false)
				{
					GinnyDeathAnimation.play(*currentGinnyDeathAnimation);
					GinnyDeathAnimationPlaying = true;

					GinnyDeathAnimation.setPosition(animatedSpriteGinnyIdle.getPosition());
					oScore1.LoadHighscores();
					oScore1.add("Hen", Score);
					/*oScore1.add("�ke", 600);
					oScore1.add("ber", 200);
					oScore1.add("And", 300);*/

					GinnyDeathAnimation.setPosition(animatedSpriteGinnyIdle.getPosition().x -25, animatedSpriteGinnyIdle.getPosition().y -25);

				}
			}

			if (GinnyDeathAnimationPlaying == true)
			{
				window.draw(GinnyDeathAnimation);
				GinnyDeathAnimation.update(deltaTime);
				GinnyDeathAnimation.move(-2, 0);
			}

			if (GinnyDeathAnimation.getFrame() == 14)
			{
				
								
				gameEstate = LOSESTATE;
				soundManager->SetLoop(false);
				soundManager->PlayMusic("../sound/music/game_over.ogg");
			}
			if (rainbowPowerBool == false)
			{
				window.draw(animatedRainbowSpritePowerUp);
			}
			if (playerRect.getGlobalBounds().intersects(rainbowPowerUpRect.getGlobalBounds()) && shieldPickUpBool == false)
			{
				rainbowPowerBool = true;

				if (powerup_isAlive == true)
				{
					soundManager->PlayEffect("Gain");
					powerup_isAlive = false;
				}
				if (tutorialSpaceActiveOnce == false)
				{
					tutorialSpaceActive = true;
					tutorialSpaceActiveOnce = true;
				}
			}
			if (playerRect.getGlobalBounds().intersects(shieldPowerUpRect.getGlobalBounds()) && rainbowPowerBool == false)
			{
				shieldPickUpBool = true;

				if (powerup_isAlive == true)
				{
					soundManager->PlayEffect("Gain");
					powerup_isAlive = false;
					shieldUsedBool = false;
				}
				if (tutorialSpaceActiveOnce == false)
				{
					tutorialSpaceActive = true;
					tutorialSpaceActiveOnce = true;
				}
			}
			shieldTimer = shieldClock.getElapsedTime();
			if (shieldPickUpBool == true && keys.isKeyPressed(sf::Keyboard::Space))
			{
				tutorialSpaceActive = false;
				animatedShieldSpritePowerUp.setPosition(-200, -200);
				shieldUsedBool = true;
				shieldClock.restart();
				shieldPickUpBool = false;

			}
			if (shieldPickUpBool == false)
			{
				window.draw(animatedShieldSpritePowerUp);
			}
			if (shieldTimer.asSeconds() < 5.0f &&shieldUsedBool==true)
			{
				shield = true;

				animatedShieldSprite.setPosition(playerPos);
				if (tylerLives == true)
				{
					TyleranimatedShieldSprite.setPosition(Tyler.getPosition());
				}
			}
			else //if (shieldTimer.asSeconds() > 5.0f && shieldUsedBool == true)
			{
				shield = false;
			}
			if (rainbowPowerBool == true && keys.isKeyPressed(sf::Keyboard::Space))
			{
				tutorialSpaceActive = false;
 				rainbowPowerActive = true;
				
 				rainbowActive = true;
				if (PlayingRainbowSound == false)
				{
					soundManager->PlayEffect("RainbowActivate");
					PlayingRainbowSound = true;
				}
			}

			if (rainbowPowerBool == false && shieldPickUpBool == false && keys.isKeyPressed(sf::Keyboard::Space))
			{
				tutorialSpaceActive = false;
				if (NoPowerUpSound == false)
				{
					soundManager->PlayEffect("NoPowerUp");
					NoPowerUpSound = true;
					NoPowerUptimer = 50;
				}
			}

			if (NoPowerUptimer >= 1)
			{
				NoPowerUptimer -= 1;
			}
			if (NoPowerUptimer == 0)
				NoPowerUpSound = false;

			if (rainbowPowerActive == true && rainbowPowerUpUsed <= 1)
			{
				rainbow.setPosition(window.getSize().x / 2, window.getSize().y / 2);
				rainbow.setOrigin(150, 150);
				rainbowPowerUsed = true;
				scale = 1.1;
				rainbow.rotate(-5);
				rainbow.scale(scale, scale);
				window.draw(rainbow);
				rainbowActive = 1;
			}
			if (rainbowPowerUpUsed >= 1)
			{
				rainbow.setScale(1, 1);
				rainbowPowerActive = false;
				rainbowPowerUsed = false;
				rainbowPowerBool = false;
				animatedRainbowSpritePowerUp.setPosition(-200, -200);

				rainbowActive = false;
				rainbowPowerUpUsed = false;

				PlayingRainbowSound = false;
				animatedSpritePowerUp.setPosition(-200, -200);
				rainbow.setPosition(-200, -200);
			}
			if (GinnyDeathAnimationPlaying == false)
			{
				animatedSpriteGinnyHeadDamaged.play(ginnyHeadDamaged);
				animatedSpriteGinnyHeadDamaged.update(deltaTime);
			}
			
			if (rainbow.getScale().x > 6 && rainbowPowerActive == true)
			{
				rainbowPowerUpUsed = 1;
			}

			for (int itRainBow = 0; itRainBow < SpawnPointsCep.size(); itRainBow++)
			{
				if (rainbowActive == 1)
				{
						if (rainbow.getGlobalBounds().intersects(SpawnPointsCep.at(itRainBow)->GetRect()))
						{
							if (SpawnPointsCep.at(itRainBow)->IsAlive == true)
							{
								SpawnPointsCep.at(itRainBow)->IsAlive = false;
								crateHitText(100, SpawnPointsCep.at(itRainBow)->GetPosition());
								mpTtl = 120;
							}
						}
				}

			}
			for (int itRainBow = 0; itRainBow < cometArray.size(); itRainBow++)
			{
				if (rainbowActive == 1)
				{
					if (rainbow.getGlobalBounds().intersects(cometArray.at(itRainBow)->GetRect()))
					{
						if (cometArray.at(itRainBow)->IsAlive == true)
						{
							AnimatedCometExplosion.play(*currentCometExpl);
							AnimatedCometExplosion.setPosition(cometArray.at(itRainBow)->GetPosition());
							cometArray.at(itRainBow)->IsAlive = false;
							crateHitText(50, cometArray.at(itRainBow)->GetPosition());
							mpTtl = 120;
						}
					}
				}
			}

			for (int itRainBow = 0; itRainBow < SpawnPointsVar.size(); itRainBow++)
			{
				if (rainbowActive == 1)
				{
						if (rainbow.getGlobalBounds().intersects(SpawnPointsVar.at(itRainBow)->GetRect()))
						{
							if (SpawnPointsVar.at(itRainBow)->IsAlive == true)
							{
								SpawnPointsVar.at(itRainBow)->IsAlive = false;
								crateHitText(200, SpawnPointsVar.at(itRainBow)->GetPosition());
								mpTtl = 120;
							}
						}
				}
			}
			
			playerRect.setPosition(playerPos);
			animatedSpriteGinnyIdle.setPosition(playerPos);
		
			if (health == 2)
			{
				healthRect.setSize(sf::Vector2f(155, 20));
				healthRect.setFillColor(sf::Color::Yellow);
			}
			if (health == 1)
			{
				healthRect.setSize(sf::Vector2f(75, 20));
				healthRect.setFillColor(sf::Color::Red);
			}
			if (health == 0)
			{
				healthRect.setSize(sf::Vector2f(0, 20));
				healthRect.setFillColor(sf::Color::Red);
			}

			if (tylerHealth == 3)
			{
				tHealthRect.setSize(sf::Vector2f(100, 15));
				tHealthRect.setFillColor(sf::Color::Yellow);
			}
			if (tylerHealth == 2)
			{
				tHealthRect.setSize(sf::Vector2f(75, 15));
				tHealthRect.setFillColor(sf::Color(255, 153, 0));
			}
			if (tylerHealth == 1)
			{
				tHealthRect.setSize(sf::Vector2f(50, 15));
				tHealthRect.setFillColor(sf::Color::Red);
			}

			//std::cout << invincibilityTime.asSeconds() << std::endl;
			if (tylerHealth == 0)
			{
				tylerLives = false;
				tHealthRect.setSize(sf::Vector2f(0, 0));
			}
		
			if (tylerLives == true)
			{
				animatedTylerSpriteIdle.setPosition(Tyler.getPosition());
				window.draw(animatedTylerSpriteIdle);
			}
			if (tylerLives == false)
			{
				window.draw(animatedTylerDeathSprite);
				animatedTylerDeathSprite.move(-2, 0);
			}
			// DRAW F�R ANIMATION
			window.draw(AnimatedCometExplosion);
	
			//Draw Cephalopods
			auto itSpawnPointCep = SpawnPointsCep.begin();
			while (itSpawnPointCep != SpawnPointsCep.end())
			{
				if ((*itSpawnPointCep)->IsAlive == true)
				{
					EnemyCepAnimation.setPosition((*itSpawnPointCep)->GetPosition());
					EnemyCepAnimation.play(*currentEnemyCepAnimation);
					EnemyCepAnimation.update(deltaTime);
					window.draw(EnemyCepAnimation);

				}

				if ((*itSpawnPointCep)->IsAlive == false)
				{

					if ((*itSpawnPointCep)->GetStartAnimationDeath() == false)
					{
						EnemyCepDeathAnimation.setPosition((*itSpawnPointCep)->GetPosition().x- 50, (*itSpawnPointCep)->GetPosition().y -50);
						
						EnemyCepDeathAnimation.play(*currentEnemyCepDetahAnimation);
						(*itSpawnPointCep)-> SetStartAnimationDeath(true);
					}
					EnemyCepDeathAnimation.update(deltaTime);
					window.draw(EnemyCepDeathAnimation);
					if (EnemyCepDeathAnimation.getFrame() == 16)
					{
						EnemyCepDeathAnimation.setFrame(4, false);
						EnemyCepDeathAnimation.setPosition(-200, -200);
						EnemyCepDeathAnimation.pause();
					}
					if (EnemyCepDeathAnimation.getPosition().x == -200)
					{
						EnemyCepDeathAnimation.stop();
						EnemyCepDeathAnimation.setFrameTime(sf::seconds(0.1));
					}
				}
					itSpawnPointCep++;
			}

			//Draw Varanidaes
			auto itSpawnPointVar = SpawnPointsVar.begin();
			while (itSpawnPointVar != SpawnPointsVar.end())
			{
				if ((*itSpawnPointVar)->IsAlive == true)
				{
					if ((*itSpawnPointVar)->IsAlive == true)
					{
						EnemyvarhAnimation.setPosition((*itSpawnPointVar)->GetPosition());
						
						EnemyvarhAnimation.play(*currentEnemyvarAnimation);
						EnemyvardmgAnimation.update(deltaTime);
						EnemyvarhAnimation.update(deltaTime);
						if ((*itSpawnPointVar)->GetHealth() != 1)
							window.draw(EnemyvarhAnimation);
						if ((*itSpawnPointVar)->GetHealth() == 1)
						{
							if (vardmgplay == false)
							{
								EnemyvardmgAnimation.setPosition((*itSpawnPointVar)->GetSprite().getPosition());
								vardmgplay = true;
								EnemyvardmgAnimation.play(*currentEnemyVarDmgAnimation);


							}
							EnemyvardmgAnimation.setPosition((*itSpawnPointVar)->GetSprite().getPosition());
							window.draw(EnemyvardmgAnimation);
						}
					}
				}
					if ((*itSpawnPointVar)->IsAlive == false)
					{
					
						if ((*itSpawnPointVar)->GetStartAnimationDeath() == false)
						{
							EnemyvarDeathAnimation.setPosition((*itSpawnPointVar)->GetPosition().x -50, (*itSpawnPointVar)->GetPosition().y -50);

							EnemyvarDeathAnimation.play(*currentEnemyvarDetahAnimation);
							(*itSpawnPointVar)->SetStartAnimationDeath(true);
						}

						
						EnemyvarDeathAnimation.update(deltaTime);
						window.draw(EnemyvarDeathAnimation);
						if (EnemyvarDeathAnimation.getFrame() == 15)
						{
							EnemyvarDeathAnimation.setFrame(4, false);
							EnemyvarDeathAnimation.setPosition(-200, -200);
							EnemyvarDeathAnimation.pause();
						}
						if (EnemyvarDeathAnimation.getPosition().x == -200)
						{
							EnemyvarDeathAnimation.stop();
							EnemyvarDeathAnimation.setFrameTime(sf::seconds(0.1));
						}
						
					}
					
				itSpawnPointVar++;
			}

			//ITARATOR F�R DRAW
			auto itercomets = cometArray.begin();
			while (itercomets != cometArray.end())
			{
				if ((*itercomets)->IsAlive == true)
				{
					firetailAnimation.setPosition((*itercomets)->GetPosition());
					firetailAnimation.play(*currentfiretailAnimation);
					firetailAnimation.update(deltaTime);
					
					window.draw(firetailAnimation);
					window.draw((*itercomets)->GetSprite());
					
				}
				itercomets++;
			}
			
			
			window.draw(ScoreText);

			//Draw pick up.
			animatedShieldSpritePowerUp.update(deltaTime);
			animatedShieldSpritePowerUp.play(*currentShieldPowerUpAnimation);

			//Draw -Ginny idle animation
			if (GinnyDeathAnimationPlaying == false)
			{
				window.draw(animatedSpriteGinnyIdle);
			}
			//Draw -Ginny Damge Sprite if hit and not dead
			if (invincibilityTime.asSeconds() <= 3.0f)
			{

				currentGinnyAnimation = &ginnyDamaged;
				animatedSpriteGinnyHeadDamaged.setPosition(ginnyHead.getPosition().x-10 , ginnyHead.getPosition().y-20);
				animatedSpriteGinnyDamaged.setOrigin(animatedSpriteGinnyHeadDamaged.getGlobalBounds().width / 2 - 5, animatedSpriteGinnyHeadDamaged.getGlobalBounds().height / 2);
				animatedSpriteGinnyHeadDamaged.setRotation(ginnyHead.getRotation());
				if (GinnyDeathAnimationPlaying == false)
				{
					window.draw(animatedSpriteGinnyHeadDamaged);
				}

			}
			else
			{
				if (GinnyDeathAnimationPlaying == false)
				{
					window.draw(ginnyHead);
				}

				currentGinnyAnimation = &ginnyIdle;
			}

			if (tInvincibilityTime.asSeconds() <= 3.0f)
			{
				currentTylerAnimation = &tylerDamaged;

			}
			else
			{
				currentTylerAnimation = &tylerIdle;
			}

			if (GinnyDeathAnimationPlaying == true)
			{
				window.draw(ginnyHead);
				ginnyHead.move(-2, 0);
				ginnyHead.rotate(-1);
			}
				//Draw -The flotingText
				for (int i = 0; i < m_hitText.size(); i++)
				{
					m_hitText.at(i)->ttl -= 1;
					m_hitText.at(i)->text.move(0, -1.5);

					if (m_hitText.at(i)->ttl >0)
					{
						window.draw(m_hitText.at(i)->text);

					}
				}

			window.draw(tylerHeadSprite);
	

				if (tylerLives == false)// && animatedTylerDeathSprite.getFrame() >= 5)
				{
					window.draw(tylerHeadSprite);
					tylerHeadSprite.move(-2, 0);
					if (animatedTylerDeathSprite.getFrame() >= 5)
					tylerHeadSprite.rotate(-1);
				}
			std::cout << mp << std::endl;


			//Draw -Ginny if Alive
			if (GinnyDeathAnimationPlaying == false)
			{
				window.draw(animatedSpriteGinny);
			}


			//Draw -The Shield
			if (shieldTimer.asSeconds() < 5.0f &&shieldUsedBool == true)
			{
				window.draw(animatedShieldSprite);
				window.draw(TyleranimatedShieldSprite);
			}

			tutorialMoveSprite.setPosition(playerPos.x, playerPos.y - 160);
			if (tutorialMoveActiveW == true || tutorialMoveActiveA == true || tutorialMoveActiveS == true || tutorialMoveActiveD == true)
			{
				window.draw(tutorialMoveSprite);
			}
			tutorialLeftSprite.setPosition(mousePos.x -100, mousePos.y - 157);
			if (tutorialLeftActive == true)
			{
				window.draw(tutorialLeftSprite);
			}

			tutorialRightSprite.setPosition(mousePos.x -100, mousePos.y - 157);
			if (tutorialRightActive == true)
			{
				window.draw(tutorialRightSprite);
			}

			tutorialSpaceSprite.setScale(0.3, 0.3);
			tutorialSpaceSprite.setPosition(510, 180);
			if (tutorialSpaceActive == true)
			{
				window.draw(tutorialSpaceSprite);
			}

			//Draw -The Hud And Lifebar.
			window.draw(healthRect);
			window.draw(tHealthRect);
			window.draw(tylerHud);
			window.draw(hud);


			//Draw HUD Shield
			if (shieldUsedBool == false && shieldPickUpBool == true)
			{
				window.draw(hudShield);
			}
			// Draw The powerUp Animation Sprite
			if (rainbowPowerBool == true && rainbowPowerUpUsed == false)
			{
				window.draw(powerUp);
			}

			//Draw SocreText on the top of the Game view
			window.draw(ScoreText);
			//Draw CrossHair 
			window.draw(CrossHair);
		}
		}
		else if (gameEstate == WINSTATE)
		{
			window.setMouseCursorVisible(true);
			window.draw(win);
			window.draw(ScoreText);
			ScoreText.setPosition(window.getSize().x / 2 - 400, window.getSize().y / 2);
			

			if (keys.isKeyPressed(sf::Keyboard::Space))
			{
				window.close();
			}
		}
		else if (gameEstate == LOSESTATE)
		{
			
			window.setMouseCursorVisible(true);
			window.draw(lose);
			window.draw(ScoreText);
			ScoreText.setPosition(window.getSize().x / 2 - 400, window.getSize().y / 2);
				if (keys.isKeyPressed(sf::Keyboard::Space))
							{
								window.close();
							}
		
			
		
		}
		window.display();
		
	}
	return 0;
}