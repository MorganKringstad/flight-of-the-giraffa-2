
#pragma once

#include <SFML\Main.hpp>
#include <SFML\Window.hpp>
#include <SFML\System.hpp>
#include <SFML\Graphics.hpp>
#include "ginny.h"



class EnemyBullet
{
public:

	EnemyBullet(sf::RectangleShape p_xBox, sf::Sprite p_Sprite, sf::RenderWindow &window);
	~EnemyBullet();
	sf::RectangleShape GetBox();
	void Update(float p_DeltaTime, sf::RenderWindow &window);
	sf::FloatRect GetRect();
	sf::Sprite GetSprite();
	void SetAnagle(float p_ValueX, float p_ValueY);
	void SetEnemyIsVar(bool value);
	bool GetEnemyIsVar();
	bool GetIsAlive();
	void SetIsAlive(bool Set_IsAlive);

	sf::RectangleShape rect;
private:

	sf::Sprite m_Sprite;
	sf::FloatRect m_Rect;
	sf::RectangleShape m_xBox;
	float angleX;
	float angleY;
	bool EnmeyIsVar;
	sf::Texture EnemyBullet2Texture;
	bool IsAlive;

};