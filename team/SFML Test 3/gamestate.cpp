#include <SFML\Main.hpp>
#include <SFML\Window.hpp>
#include <SFML\System.hpp>
#include <SFML\Graphics.hpp>
#include "ginny.h"
#include "GameState.h"

#if !defined(NDEBUG)
#pragma comment(lib, "sfml-main-d.lib")
#pragma comment(lib, "sfml-window-d.lib")
#pragma comment(lib, "sfml-system-d.lib")
#pragma comment(lib, "sfml-graphics-d.lib")
#else
#pragma comment(lib, "sfml-main.lib")
#pragma comment(lib, "sfml-window.lib")
#pragma comment(lib, "sfml-system.lib")
#pragma comment(lib, "sfml-graphics.lib")
#endif

GameState::GameState()
{

	m_pxGinny = nullptr;
}
GameState::~GameState()
{

}

void GameState::Enter()
{
	
	
	const float targetTime = 1.0f / 60.f;
	float accumalotor = 0.0f;
	float frameTime = 0.0f;
	sf::Texture ginnyTexture;
	ginnyTexture.loadFromFile("../textures/ginny1.png");
	sf::Sprite ginySprite;
	sf::Keyboard* Keyboard;
	//m_pxGinny = new ginny(Keyboard, ginySprite, 1000, 1000);
}
void GameState::Exit()
{
	m_pxGinny = nullptr;
	delete m_pxGinny;
}
bool GameState::Update(float p_fDeltaTime)
{
	sf::RenderWindow window(sf::VideoMode(1000, 1000), "sfml works!", sf::Style::Titlebar | sf::Style::Close);
	const float targetTime = 1.0f / 60.f;
	float accumalotor = 0.0f;
	float frameTime = 0.0f;
	sf::Clock clock;
	while (window.isOpen())
	{
		sf::Time deltaTime = clock.restart();
		frameTime = std::min(deltaTime.asSeconds(), 0.1f);
		accumalotor += frameTime;
		while (accumalotor > targetTime)
		{
			accumalotor -= targetTime;
			sf::Event event;
			while (window.pollEvent(event))
			{
				if (event.type == sf::Event::Closed)
				{
					window.close();
				}
			}
		}
	}
	return 0;
}