//COMET .H
#pragma once
#include <SFML\Window.hpp>
#include <SFML\System.hpp>
#include <SFML\Graphics.hpp>

class Comet
{
public:

	Comet(sf::Sprite p_Sprite, sf::Vector2f position);
	~Comet();
	sf::RectangleShape GetBox();
	void Update(float p_DeltaTime);
	sf::FloatRect GetRect();
	sf::Sprite GetSprite();
	sf::Vector2f GetPosition();
	void Comet::SetPosition(sf::Vector2f new_position);
	void SetAnagle(float p_ValueX, float p_ValueY);
	bool IsAlive;
	void ChangeAngle();
	sf::RectangleShape rect;
	bool Death();
	sf::Vector2f CometAim;

	enum CometAngleChange
	{
		First,
		Second,
		Third,
		Forth,
		Fifth,
		Sixth,
		Seventh,
		Eighth,
		Nineth,
	};

	CometAngleChange m_CometAngleChange = First;
	int NextAngle() { return m_CometAngleChange; }
	//CometAngleChange GetState();
	//void SetState(CometAngleChange p_eValue);
private:

	Comet() {}
	sf::Texture ComExplAnimation;


	sf::Sprite m_Sprite;
	sf::FloatRect m_Rect;
	sf::Vector2f m_Position;
	sf::RectangleShape m_Box;
	float angleX;
	float angleY;
	float AngleTimer;
	int SelectedAngleIndex;
	bool IsVisible;

};
