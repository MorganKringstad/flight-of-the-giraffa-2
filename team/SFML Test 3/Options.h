#pragma once
#include <SFML\Main.hpp>
#include <SFML\Window.hpp>
#include <SFML\System.hpp>
#include <SFML\Graphics.hpp>

#define MAX_NUMBER_OF_ITEMS 3
class Options
{
public:

	Options(float width, float height);
	~Options();

	void draw(sf::RenderWindow &window);
	void MoveUP();
	void MoveDown();
	int GetPressedItem() { return selectedItemIndex; }

private:

	int selectedItemIndex;
	sf::Font font;
	sf::Text options[MAX_NUMBER_OF_ITEMS];
};
