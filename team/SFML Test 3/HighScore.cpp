#include "HighScore.h"
#include <fstream>
#include <string>
#include <sstream>
#include <cstring>

HighScore::HighScore(int el)
{
	int i;
	nEl = el;
	v = new score_t*[nEl];

	for (i = 0; i<nEl; i++) 
	{
		v[i] = new score_t;
		v[i]->name = "Noname";
		v[i]->score = 0;
		v[i]->free = false; // static name
	}
}

HighScore::~HighScore()
{
	int i;

	for (i = 0; i<8; i++) 
	{
		if (v[i]->free) { // delete if dyn mem
			delete[] v[i]->name;
		}
	}
}

bool HighScore::save(char *fileName)
{
	std::ofstream oFile;
	int i;

	oFile.open(fileName);
	if (!oFile.is_open()) 
	{
		return false;
	}

	for (i = 0; i<8; i++) 
	{
		oFile << v[i]->name << ":" << v[i]->score << std::endl;
	}

	oFile.close();
	return true;
}


void HighScore::add(std::string name, int score)
{
	HighScoreItem* item = new HighScoreItem();
	item->name = name;
	item->score = score;

	//L�gg in p� r�tt plats i listan
	int i,j;
	bool scoreadded = false;
	score_t *tmp;
	for (i = 0; i < highscoreItems.size(); i++)
	{
		if (highscoreItems[i]->score > score)
		{
			continue;
			
		}
		highscoreItems.insert(highscoreItems.begin() + i, item);
		scoreadded = true;
		break;
	}
	if (!scoreadded)
	{
		highscoreItems.push_back(item);
	}
	while (highscoreItems.size() > 8)
	{
		highscoreItems.pop_back();
	}

	UpdateFile();

}
void HighScore::LoadHighscores()
{
	highscoreItems.clear();
	std::ifstream stream;
	stream.open("../Level/scorein.txt");

	if (stream.is_open())
	{
		std::string line;

		while (stream.good())
		{
			std::getline(stream, line);

			if (line.empty())
			{
				break;
			}
			int spaceIndex = line.find(":");
			std::string name = line.substr(0, spaceIndex);
			std::string score = line.substr(spaceIndex + 1, line.size());

			HighScoreItem* item = new HighScoreItem();
			item->name = name;
			item->score = std::stoi(score);

			highscoreItems.push_back(item);
		}
	}

	stream.close();
	
}
void HighScore::UpdateFile()
{
	std::ofstream stream;
	stream.open("../Level/scorein.txt");

	for (int i = 0; i < highscoreItems.size(); i++)
	{
		stream << highscoreItems[i]->name;
		stream << ":";
		stream << highscoreItems[i]->score;
		stream << "\n";
	}


	stream.close();
}

void HighScore::print(std::ostream &out)
{
	int i;

	for (i = 0; i<nEl; i++) {
		out << v[i]->name << ":" << v[i]->score << std::endl;
	}
}
