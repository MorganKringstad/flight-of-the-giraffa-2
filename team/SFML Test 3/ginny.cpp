#include <SFML\Main.hpp>
#include <SFML\Window.hpp>
#include <SFML\System.hpp>
#include <SFML\Graphics.hpp>
#include "ginny.h"

ginny::ginny(sf::Keyboard* p_pxKeyboard,sf::Sprite* p_pxSprite,int p_ScreenWidth,int p_ScreenHeight)
{
	m_pxSpite = p_pxSprite;
	m_pxKeyboard = p_pxKeyboard;
	m_Speed = 0.0f;
	m_ScreenWidth = p_ScreenWidth;
	m_ScreenHeight = p_ScreenHeight;
	m_Position = sf::Vector2f(0.0f,0.0f);
	m_Active = true;
	m_pxRect = new sf::FloatRect(m_pxSpite->getGlobalBounds());
}

ginny::~ginny()
{
	delete m_pxRect;
	m_pxRect = nullptr;
}

void ginny::Update(float p_DeltaTime)
{
	if (m_Position.x < 0)
	{
		m_Position.x = 0;
	}
	if (m_Position.x + m_pxSpite->getGlobalBounds().width>m_ScreenWidth)
	{
		m_Position.x = m_ScreenWidth - m_pxSpite->getGlobalBounds().width;
	}
	if (m_Position.y < 0)
	{
		m_Position.y = 0;
	}
	if (m_Position.y + m_pxSpite->getGlobalBounds().height>m_ScreenHeight)
	{
		m_Position.y = m_ScreenHeight - m_pxSpite->getGlobalBounds().height;
	}
	if (m_pxKeyboard->isKeyPressed(sf::Keyboard().W))
	{
		m_Position.y--;
	}
	if (m_pxKeyboard->isKeyPressed(sf::Keyboard().S))
	{
		m_Position.y++;
	}
	if (m_pxKeyboard->isKeyPressed(sf::Keyboard().A))
	{
		m_Position.x--;
	}
	if (m_pxKeyboard->isKeyPressed(sf::Keyboard().D))
	{
		m_Position.x++;
	}
}

sf::Sprite* ginny::GetSprite()
{
	return m_pxSpite;
}

sf::FloatRect* ginny::GetRect()
{
	return m_pxRect;
}

sf::Vector2f ginny::GetPosition()
{
	return m_Position;
}

bool ginny::IsActive()
{
	return m_Active;
}

EENTITYTYPE ginny::GetType()
{
	return EENTITYTYPE::ENTITY_GINNY;
}