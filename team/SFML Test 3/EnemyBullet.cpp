#include "EnemyBullet.h"
#include <SFML\Main.hpp>
#include <SFML\Window.hpp>
#include <SFML\System.hpp>
#include <SFML\Graphics.hpp>
#include "ginny.h"

#define _USE_MATH_DEFINES
#include <math.h>

EnemyBullet::EnemyBullet(sf::RectangleShape p_xBox, sf::Sprite p_SPrite, sf::RenderWindow &window)
{
	EnemyBullet2Texture.loadFromFile("../textures/enemybullet1.png");
	m_xBox = p_xBox;
	m_Sprite = p_SPrite;
	IsAlive = true;

	rect.setFillColor(sf::Color::Green);
	rect.setSize(sf::Vector2f(30, 30));
	angleX = 0;
	angleY = 0;
	m_Sprite.setOrigin(10, 5);
	EnmeyIsVar = false;

	

}
EnemyBullet::~EnemyBullet()
{

}
sf::RectangleShape EnemyBullet::GetBox()
{
	return m_xBox;
}
sf::FloatRect EnemyBullet::GetRect()
{
	m_Rect = rect.getGlobalBounds();
	return m_Rect;
}
sf::Sprite EnemyBullet::GetSprite()
{
	
	return m_Sprite;
}
void EnemyBullet::Update(float p_DeltaTime, sf::RenderWindow &window)
{
	if (EnmeyIsVar == true)
	{
		m_Sprite.setOrigin(10, 10);
		m_Sprite.setTexture(EnemyBullet2Texture);
	}
	m_Sprite.setPosition(rect.getPosition());
	m_Sprite.rotate(-5);

	rect.move(cos(angleX * M_PI / 180.f)*8.0f, sin(angleY * M_PI / 180.f)*8.0f);


}

void EnemyBullet::SetAnagle(float p_ValueX, float p_ValueY)
{
	angleX = p_ValueX;
	angleY = p_ValueY;
}

void EnemyBullet::SetEnemyIsVar(bool value) { EnmeyIsVar = value; }
bool EnemyBullet::GetEnemyIsVar() { return EnmeyIsVar; }
bool EnemyBullet::GetIsAlive() { return IsAlive; }
void EnemyBullet::SetIsAlive(bool Set_IsAlive) { IsAlive = Set_IsAlive; }