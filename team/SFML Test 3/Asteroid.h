#pragma once
#include <SFML\Main.hpp>
#include <SFML\Window.hpp>
#include <SFML\System.hpp>
#include <SFML\Graphics.hpp>
class Asteroid
{
public:
	Asteroid(sf::Sprite iSprite,sf::Vector2f p_Pos);
	~Asteroid();
	sf::FloatRect GetRect();
	void SetPos(sf::Vector2f newPos);
	sf::Vector2f GetPos();
	void Update(float p_deltaTime);
	sf::Sprite GetSprite();
private:
	sf::FloatRect m_Rect;
	sf::Vector2f m_Position;
	sf::Sprite m_Sprite;


	
};