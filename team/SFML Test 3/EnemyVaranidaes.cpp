#include "EnemyVaranidaes.h"
#include <SFML\Main.hpp>
#include <SFML\Window.hpp>
#include <SFML\System.hpp>
#include <SFML\Graphics.hpp>
#include "SoundManager.h"
#include "AnimatedSprite.hpp"



EnemyVaranidaes::EnemyVaranidaes(sf::Vector2f p_Position, sf::Sprite p_Sprite)
{

	
	EnemyVarTextureDamge.loadFromFile("../textures/CephalopodsTemp.png");
	m_Sprite = p_Sprite;
	m_Position = p_Position;
	m_isActive = true;
	m_deathTimer = 0;
	m_healLeft = 2;
	m_move = 0;
	m_moveTimer = 0;
	m_shotTimer = 0;
	IsAlive = true;
	m_Sprite.setPosition(m_Position);
	FireAngle1 = 0;
	FireAray = 0;
	
	


}

EnemyVaranidaes::~EnemyVaranidaes()
{

}

bool EnemyVaranidaes::isActive()
{
	return 1;
}
sf::FloatRect EnemyVaranidaes::GetRect()
{
	m_rect = m_Sprite.getGlobalBounds();
	return m_rect;
}
sf::Sprite EnemyVaranidaes::GetSprite()
{
	return m_Sprite;
}

void EnemyVaranidaes::SetTexture(sf::Texture New_texture) { m_Sprite.setTexture(New_texture); }

void EnemyVaranidaes::Update(float p_DeltaTime)
{

	switch (m_EnemyVaranidaesState)
	{
	case EnemyVaranidaes::INACTIVE:
		//Check if EnemyVaranidaes is on Screen Change State
		break;
	case EnemyVaranidaes::ACTIVE:

		if (move_patterns = true)
		{
			if (m_Sprite.getPosition().x <= 1980) // If The Enemy is in the Avatar Area
			{
				if (m_Sprite.getPosition().x <= 1980 && m_Sprite.getPosition().x >= 1620)
					m_move = 4;

				if (m_Sprite.getPosition().x == 1620)
				{
					m_move = 4;
					// First movment patern
					if (m_Sprite.getPosition().y <= 540 && m_Sprite.getPosition().y >= 10)
					{
						m_move = 2;
					}

					if (m_Sprite.getPosition().y <= 8)
						m_move = 10;

					if (m_Sprite.getPosition().y == 9)
					{
						m_move = 4;
					}


					//Secoun movment bleow
					if (m_Sprite.getPosition().y >= 540)
					{
						m_move = 1;
					}

					if (m_Sprite.getPosition().y == 940)
					{
						m_move = 4;
					}
					if (m_Sprite.getPosition().y >= 941)
					{
						m_move = 11;
					}

				}

				// First movment patern
				if (m_Sprite.getPosition().x == 1420 && m_Sprite.getPosition().y == 9)
				{
					m_move = 1;
				}
				if (m_Sprite.getPosition().x == 1420 && m_Sprite.getPosition().y == 953)
				{
					m_move = 4;
				}

				if (m_Sprite.getPosition().x == 920 && m_Sprite.getPosition().y == 953)
				{
					m_move = 2;
				}

				if (m_Sprite.getPosition().x == 920 && m_Sprite.getPosition().y == 9)
				{
					m_move = 4;
				}

				if (m_Sprite.getPosition().x == 460 && m_Sprite.getPosition().y == 953)
				{
					m_move = 4;
				}

				if (m_Sprite.getPosition().x == 460 && m_Sprite.getPosition().y == 9)
				{
					m_move = 1;
				}

				//Secound
				if (m_Sprite.getPosition().x == 1520 && m_Sprite.getPosition().y == 32)
				{
					m_move = 4;
				}
				if (m_Sprite.getPosition().x == 1520 && m_Sprite.getPosition().y == 940)
				{
					m_move = 2;
				}

				if (m_Sprite.getPosition().x == 1020 && m_Sprite.getPosition().y == 940)
				{
					m_move = 4;
				}

				if (m_Sprite.getPosition().x == 1020 && m_Sprite.getPosition().y == 32)
				{
					m_move = 1;
				}

				if (m_Sprite.getPosition().x == 560 && m_Sprite.getPosition().y == 940)
				{
					m_move = 2;
				}

				if (m_Sprite.getPosition().x == 560 && m_Sprite.getPosition().y == 32)
				{
					m_move = 4;
				}
				
			}
			if (move_patterns == false)
			{
				if (m_Sprite.getPosition().y <= 540);
				m_move = 1;
				m_moveTimer++;
				if (m_moveTimer >= 50)
				{
					m_move = 7;
				}

				else if (m_moveTimer <= 50) // Does this work?
				{
					m_move = 6;
					//Rect_enemyCephalopods.move(-2, -2); Move left and down
				}

				if (m_moveTimer >= 100)
				{
					m_moveTimer = 0;
					//Reset
				}
			}

			if (m_Sprite.getPosition().x >= 1980)
			{
				m_move = 4;
			}

			//Shots
			m_shotTimer++;
			if (m_shotTimer == 150)
			{
				normalShot();

			}
			//if (m_shotTimer == 160)
			//{
			//	normalShot();

			//}
			if (m_shotTimer == 170)
			{
				normalShot();

			}
			//if (m_shotTimer == 180)
			//{
			//	normalShot();

			//}
			if (m_shotTimer == 190)
			{
				normalShot();
				m_shotTimer = 0;

			}
			
		}

		////if (m_Sprite.getPosition().x <= 0)
		////{
		////	m_Sprite.setPosition(1900, m_Position.y);
		////}

		//Movement
		if (m_move == 0)
		{
			m_Sprite.move(0, 0);
		}
		if (m_move == 1)
		{
			m_Sprite.move(0, 4);
		}
		if (m_move == 2)
		{
			m_Sprite.move(0, -4);
		}
		if (m_move == 3)
		{
			m_Sprite.move(4, 0);
		}
		if (m_move == 4)
		{
			m_Sprite.move(-4, 0);
		}
		if (m_move == 5)
		{
			m_Sprite.move(4, 4);
		}
		if (m_move == 6)
		{
			m_Sprite.move(-4, 4);
		}
		if (m_move == 7)
		{
			m_Sprite.move(-4, -4);
		}
		if (m_move == 8)
		{
			m_Sprite.move(4, -4);
		}
		if (m_move == 10)
		{
			m_Sprite.move(0, 1);
		}
		if (m_move == 11)
		{
			m_Sprite.move(0, -1);
		}
		if (m_move == 12)
			m_Sprite.move(-2, 0);
		//Movemet-end
		
		if (m_healLeft == 1)
		{
			//m_Sprite.setTexture(EnemyVarTextureDamge);
		}

		if (m_healLeft <= 0)
		{
			IsAlive = false;
		}

		if (IsAlive == false)
		{
			m_EnemyVaranidaesState = DeathState;
		}
		break;
	case EnemyVaranidaes::DeathState:
		m_Sprite.setRotation(90);
		IsAlive = false;
		break;
	}
}
void EnemyVaranidaes::SetStartAnimationDeath(bool Value)
{
	Start_animationDeath = Value;
}
bool EnemyVaranidaes::GetStartAnimationDeath() { return Start_animationDeath; }
sf::Vector2f EnemyVaranidaes::GetPosition() { return m_Sprite.getPosition(); }

bool EnemyVaranidaes::GetFireAray() { return FireAray; }
bool EnemyVaranidaes::GetIsAlive() { return IsAlive; }
int EnemyVaranidaes::GetHealth() { return m_healLeft; }
void EnemyVaranidaes::SetHealthLeft(int Set_Health) { m_healLeft = Set_Health; }
void EnemyVaranidaes::SetFireAray(bool Set_FireAray) { FireAray = Set_FireAray; }
void EnemyVaranidaes::SetPosition(sf::Vector2f new_Position) { m_Position = new_Position; }

void EnemyVaranidaes::Death()
{
	m_rect = sf::RectangleShape(sf::Vector2f(0, 0)).getGlobalBounds();
}
sf::RectangleShape EnemyVaranidaes::GetBox()
{
	return Rect_EnemyVaranidaes;
}
void EnemyVaranidaes::normalShot()
{
	FireAray = 1;

}


void EnemyVaranidaes::draw(sf::RenderWindow* window)
{
	window->draw(m_Sprite);
}


EnemyVaranidaes::EnemyVaranidaesState EnemyVaranidaes::GetState() { return m_EnemyVaranidaesState; }
void EnemyVaranidaes::SetState(EnemyVaranidaesState p_eValue) { m_EnemyVaranidaesState = p_eValue; }
void EnemyVaranidaes::SetPatternes(bool p_eValue) { move_patterns = p_eValue; }
bool EnemyVaranidaes::GetPatternes() { return move_patterns; }

int EnemyVaranidaes::getDeathTimer() { return m_deathTimer; }
