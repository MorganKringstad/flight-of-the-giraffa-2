#include <SFML\Main.hpp>
#include <SFML\Window.hpp>
#include <SFML\System.hpp>
#include <SFML\Graphics.hpp>
#include "Options.h"

Options::Options(float width, float height)
{
	if (!font.loadFromFile("../textures/TOMOPRG_.TTF"))
	{
		//handle error
	}
	options[0].setFont(font);
	options[0].setColor(sf::Color::Yellow);
	options[0].setString("FullScreen?");
	options[0].setPosition(sf::Vector2f(width / 6, height / (MAX_NUMBER_OF_ITEMS + 1) * 1));

	options[1].setFont(font);
	options[1].setColor(sf::Color::White);
	options[1].setString("HighScore");
	options[1].setPosition(sf::Vector2f(width / 6, height / (MAX_NUMBER_OF_ITEMS + 1) * 2));

	options[2].setFont(font);
	options[2].setColor(sf::Color::White);
	options[2].setString("Back");
	options[2].setPosition(sf::Vector2f(width / 6, height / (MAX_NUMBER_OF_ITEMS + 1) * 3));

	selectedItemIndex = 0;
}

Options::~Options()
{

}

void Options::draw(sf::RenderWindow &window)
{
	for (int i = 0; i < MAX_NUMBER_OF_ITEMS; i++)
	{
		window.draw(options[i]);
	}
}

void Options::MoveUP()
{
	if (selectedItemIndex - 1 >= 0)
	{
		options[selectedItemIndex].setColor(sf::Color::White);
		selectedItemIndex--;
		options[selectedItemIndex].setColor(sf::Color::Yellow);
	}
}

void Options::MoveDown()
{
	if (selectedItemIndex + 1 < MAX_NUMBER_OF_ITEMS)
	{
		options[selectedItemIndex].setColor(sf::Color::White);
		selectedItemIndex++;
		options[selectedItemIndex].setColor(sf::Color::Yellow);
	}
}