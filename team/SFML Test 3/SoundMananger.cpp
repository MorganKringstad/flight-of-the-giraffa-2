#include "SoundManager.h"
#include <SFML\Audio.hpp>
#include <SFML\Main.hpp>
#include <string.h>
#include <map>
#include <iostream>
//
//
//
//SoundManager::SoundManager()
//{
//
//}
//
//SoundManager::~SoundManager()
//{
//
//}
//
//void SoundManager::loadEffect(std::string p_loadEffectPath)
//{
//
//	auto it = m_SFXMap.find(p_loadEffectPath.c_str());
//	if (it == m_SFXMap.end())
//	{
//		
//		if (!Buffer.loadFromFile(p_loadEffectPath))
//		{
//			std::cout << "load Form File not" << std::endl;
//		}
//
//		it = m_SFXMap.find(p_loadEffectPath);
//
//	}
//}
//
//
//void SoundManager::LoadMusic(std::string p_MusicPath)
//{
//
//	auto it = m_musicMap.find(p_MusicPath.c_str());
//	if (it == m_musicMap.end())
//	{
//		if (!music.openFromFile(p_MusicPath))
//		{
//			std::cout << "load Form File not" << std::endl;
//		}
//
//		it = m_musicMap.find(p_MusicPath);
//
//	}
//	music.setLoop(true);
//}
//
//
//void SoundManager::PlayEffect(std::string p_EffectPath)
//{
//	loadEffect(p_EffectPath);
//	sound.setBuffer(Buffer);
//	if (sound.getStatus() != sf::Sound::Playing)
//	{
//		int random = std::rand() % 3;
//		if (random == 0)
//		{
//			sound.setPitch(1.1);
//			sound.play();
//		}
//		else if (random == 1)
//		{
//			sound.setPitch(0.9);
//			sound.play();
//		}
//		else
//		{
//			sound.setPitch(1);
//			sound.play();
//		}
//
//
//	}
//	
//}
//
//void SoundManager::PlayMusic(std::string p_MusicPath)
//{
//	LoadMusic(p_MusicPath);
//	//sound.setBuffer(Buffer);
//	music.play();
//}




SoundManager::SoundManager()
{
	loops = true;
}

SoundManager::~SoundManager()
{

}

void SoundManager::loadEffect()
{
		if (!bufferplop.loadFromFile("../sound/effect/avatar_projectile.ogg"))
		{
			std::cout << "load Form File not working - plop" << std::endl;
		}

		if (!bufferGainPower.loadFromFile("../sound/effect/gaining_power_up.ogg"))
		{
			std::cout << "Load from File not working - Gain_power_up" << std::endl;
		}

		if (!bufferPause.loadFromFile("../sound/effect/pause_game.ogg"))
		{
			std::cout << "Load from File not working - Gain_power_up" << std::endl;
		}

		if (!bufferjetpack.loadFromFile("../sound/effect/jetpack.ogg"))
		{
			std::cout << "Load from File not working - Gain_power_up" << std::endl;
		}
		if (!bufferenemyDeath.loadFromFile("../sound/effect/enemy_death.ogg"))
		{
			std::cout << "Load from File not working - enemy_death" << std::endl;
		}
		if (!bufferginnyTakingDamage.loadFromFile("../sound/effect/ginny_taking_damage.ogg"))
		{
			std::cout << "Load from File not working - ginny_taking_damage" << std::endl;
		}
		if (!bufferkometExplosion.loadFromFile("../sound/effect/komet_explosion.ogg"))
		{
			std::cout << "Load from File not working - komet_explosion" << std::endl;
		}
		if (!bufferRainbowActivate.loadFromFile("../sound/effect/rainbow_activate.ogg"))
		{
			std::cout << "Load from File not working - rainbow_activate" << std::endl;
		}
		if (!buffershieldActivate.loadFromFile("../sound/effect/shield_activate.ogg"))
		{
			std::cout << "Load from File not working - shield_activate" << std::endl;
		}
		if (!buffertylerTakingDamage.loadFromFile("../sound/effect/tyler_taking_damage.ogg"))
		{
			std::cout << "Load from File not working - tyler_taking_damage" << std::endl;
		}
		if (!bufferHoveringMeny.loadFromFile("../sound/effect/hovering_over_button.ogg"))
		{
			std::cout << "Load from File not working - hovering_over_button" << std::endl;
		}
		if (!bufferNoPowerUp.loadFromFile("../sound/effect/not_having_a_power_up.ogg"))
		{
			std::cout << "Load from File not working - not_having_a_power_up" << std::endl;
		}
		if (!bufferGttingHit.loadFromFile("../sound/effect/getting_hit.ogg"))
		{
			std::cout << "Load from File not working - getting_hit" << std::endl;
		}
		if (!bufferclick.loadFromFile("../sound/effect/clicking_a_button.ogg"))
		{
			std::cout << "Load from File not working - getting_hit" << std::endl;
		}
}


void SoundManager::LoadMusic(std::string p_MusicPath)
{

	auto it = m_musicMap.find(p_MusicPath.c_str());
	if (it == m_musicMap.end())
	{
		if (!music.openFromFile(p_MusicPath))
		{
			std::cout << "load Form File not" << std::endl;
		}

		it = m_musicMap.find(p_MusicPath);

	}
	music.setLoop(loops);
}

bool SoundManager::GetLoop() { return loops; }
void SoundManager::SetLoop(bool value) { loops = value; }

void SoundManager::update()
{
	for (auto iSound = vSounds_.begin(); iSound != vSounds_.end();)
		if (iSound->getStatus() == sf::Sound::Stopped)
			iSound = vSounds_.erase(iSound);
		else
			++iSound;

}


void SoundManager::PlayEffect(const std::string& type)
{
	if (type == "plop")
	{
		sf::Sound sound;
		int random = std::rand() % 3;
		if (random == 0)
		{

			sound = sf::Sound(bufferplop);
			sound.setPitch(1.2);
		}
		else if (random == 1)
		{

			sound = sf::Sound(bufferplop);
			sound.setPitch(0.8);
		}
		else
		{

			sound = sf::Sound(bufferplop);
			sound.setPitch(1);
		}
		sound.setVolume(50);
		vSounds_.push_back(sound);
		vSounds_.back().play();
	}
	if (type == "Tylerplop")
	{
		sf::Sound sound;
		int random = std::rand() % 3;
		if (random == 0)
		{

			sound = sf::Sound(bufferplop);
			sound.setPitch(0.6);
		}
		else if (random == 1)
		{

			sound = sf::Sound(bufferplop);
			sound.setPitch(0.5);
		}
		else
		{

			sound = sf::Sound(bufferplop);
			sound.setPitch(0.4);
		}
		sound.setVolume(50);
		vSounds_.push_back(sound);
		vSounds_.back().play();
	}
	if (type == "Gain")
	{
		
		sf::Sound sound;
		int random = std::rand() % 3;
		if (random == 0)
		{

			sound = sf::Sound(bufferGainPower);
			sound.setPitch(1.1);
		}
		else if (random == 1)
		{

			sound = sf::Sound(bufferGainPower);
			sound.setPitch(0.9);
		}
		else
		{

			sound = sf::Sound(bufferGainPower);
			sound.setPitch(1);
		}
		
			vSounds_.push_back(sound);
			vSounds_.back().play();
	}

	if (type == "eDeath")
	{

		sf::Sound sound;
		int random = std::rand() % 3;
		if (random == 0)
		{

			sound = sf::Sound(bufferenemyDeath);
			sound.setPitch(1.1);
		}
		else if (random == 1)
		{

			sound = sf::Sound(bufferenemyDeath);
			sound.setPitch(0.9);
		}
		else
		{

			sound = sf::Sound(bufferenemyDeath);
			sound.setPitch(1);
		}

		vSounds_.push_back(sound);
		vSounds_.back().play();
	}

	if (type == "ginnyTakingDamage")
	{

		sf::Sound sound;
		int random = std::rand() % 3;
		if (random == 0)
		{

			sound = sf::Sound(bufferginnyTakingDamage);
			sound.setPitch(1.1);
		}
		else if (random == 1)
		{

			sound = sf::Sound(bufferginnyTakingDamage);
			sound.setPitch(0.9);
		}
		else
		{

			sound = sf::Sound(bufferginnyTakingDamage);
			sound.setPitch(1);
		}

		vSounds_.push_back(sound);
		vSounds_.back().play();
	}

	
		if (type == "kometExplosion")
		{

			sf::Sound sound;
			int random = std::rand() % 3;
			if (random == 0)
			{

				sound = sf::Sound(bufferkometExplosion);
				sound.setPitch(1.1);
			}
			else if (random == 1)
			{

				sound = sf::Sound(bufferkometExplosion);
				sound.setPitch(0.9);
			}
			else
			{

				sound = sf::Sound(bufferkometExplosion);
				sound.setPitch(1);
			}

			vSounds_.push_back(sound);
			vSounds_.back().play();
		}

			if (type == "RainbowActivate")
			{

				sf::Sound sound;
				int random = std::rand() % 3;
				if (random == 0)
				{

					sound = sf::Sound(bufferRainbowActivate);
					sound.setPitch(1.1);
				}
				else if (random == 1)
				{

					sound = sf::Sound(bufferRainbowActivate);
					sound.setPitch(0.9);
				}
				else
				{

					sound = sf::Sound(bufferRainbowActivate);
					sound.setPitch(1);
				}

				vSounds_.push_back(sound);
				vSounds_.back().play();
			}

			

				if (type == "shieldActivate")
				{

					sf::Sound sound;
					int random = std::rand() % 3;
					if (random == 0)
					{

						sound = sf::Sound(buffershieldActivate);
						sound.setPitch(1.1);
					}
					else if (random == 1)
					{

						sound = sf::Sound(buffershieldActivate);
						sound.setPitch(0.9);
					}
					else
					{

						sound = sf::Sound(buffershieldActivate);
						sound.setPitch(1);
					}

					vSounds_.push_back(sound);
					vSounds_.back().play();
				}

				if (type == "tylerTakingDamage")
				{

					sf::Sound sound;
					int random = std::rand() % 3;
					if (random == 0)
					{

						sound = sf::Sound(buffertylerTakingDamage);
						sound.setPitch(1.1);
					}
					else if (random == 1)
					{

						sound = sf::Sound(buffertylerTakingDamage);
						sound.setPitch(0.9);
					}
					else
					{

						sound = sf::Sound(buffertylerTakingDamage);
						sound.setPitch(1);
					}

					vSounds_.push_back(sound);
					vSounds_.back().play();
				}
				
				if (type == "GttingHit")
				{

					sf::Sound sound;
					int random = std::rand() % 3;
					if (random == 0)
					{

						sound = sf::Sound(bufferGttingHit);
						sound.setPitch(1.1);
					}
					else if (random == 1)
					{

						sound = sf::Sound(bufferGttingHit);
						sound.setPitch(0.9);
					}
					else
					{

						sound = sf::Sound(bufferGttingHit);
						sound.setPitch(1);
					}

					vSounds_.push_back(sound);
					vSounds_.back().play();
				}

				if (type == "NoPowerUp")
				{

					sf::Sound sound;
					int random = std::rand() % 3;
					if (random == 0)
					{

						sound = sf::Sound(bufferNoPowerUp);
						sound.setPitch(1.1);
					}
					else if (random == 1)
					{

						sound = sf::Sound(bufferNoPowerUp);
						sound.setPitch(0.9);
					}
					else
					{

						sound = sf::Sound(bufferNoPowerUp);
						sound.setPitch(1);
					}

					vSounds_.push_back(sound);
					vSounds_.back().play();
				}

	if (type == "pause")
	{

		sf::Sound sound;
		

			sound = sf::Sound(bufferPause);

		vSounds_.push_back(sound);
		vSounds_.back().play();
	}

	if (type == "click")
	{

		sf::Sound sound;


		sound = sf::Sound(bufferclick);

		vSounds_.push_back(sound);
		vSounds_.back().play();
	}

	if (type == "hover")
	{

		sf::Sound sound;


		sound = sf::Sound(bufferHoveringMeny);
		sound.setVolume(50);
		vSounds_.push_back(sound);
		vSounds_.back().play();
	}

	if (type == "jatpack")
	{

		sf::Sound sound;


		sound = sf::Sound(bufferjetpack);
		sound.setVolume(20);
		sound.setLoop(true);
		vSounds_.push_back(sound);
		vSounds_.back().play();
	}
}

void SoundManager::PlayMusic(std::string p_MusicPath)
{
	LoadMusic(p_MusicPath);
	//sound.setBuffer(Buffer);
	music.play();
}