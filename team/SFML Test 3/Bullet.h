#pragma once
#include <SFML\Main.hpp>
#include <SFML\Window.hpp>
#include <SFML\System.hpp>
#include <SFML\Graphics.hpp>
#include "ginny.h"

class Bullet
{
public:
	
	Bullet(sf::RectangleShape p_xBox,sf::Sprite p_Sprite, sf::RenderWindow &window);
	~Bullet();
	sf::RectangleShape GetBox();
	void Update(float p_DeltaTime, sf::RenderWindow &window, sf::Vector2f playerPos);
	void SetAngle(float newAngle);
	sf::FloatRect GetRect();
	sf::Sprite GetSprite();
	void SetBulletRotation(float bulletRotation);
	bool destroy;
	sf::Vector2f mousePos2;
	sf::RectangleShape rect;
	bool GetIsAlive();
	void SetIsAlive(bool Set_IsAlive);

private:


	sf::Sprite m_Sprite;
	sf::FloatRect m_Rect;
	sf::Vector2f m_PlayerPos;
	sf::RectangleShape m_xBox;
	float m_Angle;
	float m_BulletRotation;
	bool IsAlive;
	

}; 
	