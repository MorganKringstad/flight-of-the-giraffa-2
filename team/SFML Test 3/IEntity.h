#pragma once
#include <SFML\Main.hpp>
#include <SFML\Window.hpp>
#include <SFML\System.hpp>
#include <SFML\Graphics.hpp>
#include <iostream>

enum EENTITYTYPE
{
	ENTITY_LASER,
	ENTITY_GINNY
};


class IEntity
{
public:
	~IEntity() {};
	virtual void Update(float DeltaTime) = 0;
	virtual sf::Sprite* GetSprite() { return nullptr; }// Can return nullptr if there is no sprite
	virtual sf::FloatRect* GetRect() = 0;
	virtual sf::Vector2f* GetPosition() = 0;
	virtual EENTITYTYPE GetType() = 0;
};