//COMET CPP

#include <SFML\Window.hpp>
#include <SFML\System.hpp>
#include <SFML\Graphics.hpp>
#include "Comet.h"

#define _USE_MATH_DEFINES
#include <math.h>

Comet::Comet(sf::Sprite p_pxSprite, sf::Vector2f p_position)
{

	m_Sprite = p_pxSprite;
	m_Position = p_position;
	m_Sprite.setPosition(m_Position);
	rect.setFillColor(sf::Color::Red);
	rect.setSize(sf::Vector2f(160, 160));
	float angleX = 180;
	float angleY = 5;
	IsAlive = true;
	IsVisible = true;
	SetAnagle(180, 180);
	m_Sprite.setOrigin(80, 80);

}

Comet::~Comet()
{

}

sf::RectangleShape Comet::GetBox()
{
	return m_Box;
}
void Comet::Update(float p_DeltaTime)
{
	/*float angle2 = 5.0f;*/
	/*atan2(angleX, angleY);*/
	m_Sprite.rotate(0.7);
	if (IsAlive == true)
	{

		if (m_Sprite.getPosition().x < 1920)
		{

			m_Sprite.move(cos(angleX * M_PI / 180.f)*3.0f, sin(angleY * M_PI / 180.f)*3.0f);
			/*AngleTimer = m_Sprite.getPosition().x;*/

			switch (m_CometAngleChange)
			{
			case Comet::First:
				SetAnagle(180, 190);

				break;
			case Comet::Second:
				SetAnagle(180, 188);

				break;
			case Comet::Third:
				SetAnagle(180, 183);

				break;
			case Comet::Forth:
				SetAnagle(180, 178);

				break;
			case Comet::Fifth:
				SetAnagle(180, 175);

				break;
			case Comet::Sixth:
				SetAnagle(180, 172);

				break;
			case Comet::Seventh:
				SetAnagle(180, 168);

				break;
			case Comet::Eighth:
				SetAnagle(180, 165);

				break;
			case Comet::Nineth:
				SetAnagle(180, 161);

				break;

			default:
				break;
			}

			if (m_Sprite.getPosition().x < 1900)
			{

				m_CometAngleChange = Second;
			}
			if (m_Sprite.getPosition().x < 1800)
			{

				m_CometAngleChange = Third;
			}
			if (m_Sprite.getPosition().x < 1700)
			{

				m_CometAngleChange = Forth;
			}
			if (m_Sprite.getPosition().x < 1600)
			{

				m_CometAngleChange = Fifth;
			}
			if (m_Sprite.getPosition().x < 1500)
			{

				m_CometAngleChange = Sixth;
			}
			if (m_Sprite.getPosition().x < 1400)
			{

				m_CometAngleChange = Seventh;
			}
			if (m_Sprite.getPosition().x < 1300)
			{

				m_CometAngleChange = Eighth;
			}
			if (m_Sprite.getPosition().x < 1200)
			{
				m_CometAngleChange = Nineth;
			}

		}
		else
		{
			m_Sprite.move(cos(angleX * M_PI / 180.f)*3.0f, sin(angleY * M_PI / 180.f)*3.0f);
			SetAnagle(180, 180);
		}

		/*m_Sprite.setPosition(rect.getPosition());*/
	}
	if (IsAlive == false)
	{

		//test

		if (m_Sprite.getPosition().x < 1920)
		{

			m_Sprite.move(cos(angleX * M_PI / 180.f)*3.0f, sin(angleY * M_PI / 180.f)*3.0f);
			/*AngleTimer = m_Sprite.getPosition().x;*/

			switch (m_CometAngleChange)
			{
			case Comet::First:
				SetAnagle(180, 190);

				break;
			case Comet::Second:
				SetAnagle(180, 188);

				break;
			case Comet::Third:
				SetAnagle(180, 183);

				break;
			case Comet::Forth:
				SetAnagle(180, 178);

				break;
			case Comet::Fifth:
				SetAnagle(180, 175);

				break;
			case Comet::Sixth:
				SetAnagle(180, 172);

				break;
			case Comet::Seventh:
				SetAnagle(180, 168);

				break;
			case Comet::Eighth:
				SetAnagle(180, 165);

				break;
			case Comet::Nineth:
				SetAnagle(180, 161);

				break;

			default:
				break;
			}

			if (m_Sprite.getPosition().x < 1900)
			{

				m_CometAngleChange = Second;
			}
			if (m_Sprite.getPosition().x < 1800)
			{

				m_CometAngleChange = Third;
			}
			if (m_Sprite.getPosition().x < 1700)
			{

				m_CometAngleChange = Forth;
			}
			if (m_Sprite.getPosition().x < 1600)
			{

				m_CometAngleChange = Fifth;
			}
			if (m_Sprite.getPosition().x < 1500)
			{

				m_CometAngleChange = Sixth;
			}
			if (m_Sprite.getPosition().x < 1400)
			{

				m_CometAngleChange = Seventh;
			}
			if (m_Sprite.getPosition().x < 1300)
			{

				m_CometAngleChange = Eighth;
			}
			if (m_Sprite.getPosition().x < 1200)
			{
				m_CometAngleChange = Nineth;
			}

		}
		else
		{
			m_Sprite.move(cos(angleX * M_PI / 180.f)*3.0f, sin(angleY * M_PI / 180.f)*3.0f);
			SetAnagle(180, 180);
		}
		// Test
	}

}

sf::Sprite Comet::GetSprite()
{
	return m_Sprite;
}
sf::Vector2f Comet::GetPosition()
{
	return m_Sprite.getPosition();
}

void Comet::SetPosition(sf::Vector2f new_position)
{
	m_Position = new_position;
}
void Comet::SetAnagle(float p_ValueX, float p_ValueY)
{
	angleX = p_ValueX;
	angleY = p_ValueY;
}

bool Comet::Death()
{
	m_Rect = sf::RectangleShape(sf::Vector2f(0, 0)).getGlobalBounds();

	return IsAlive = false;
	return IsVisible = false;
}

void Comet::ChangeAngle()
{


}

sf::FloatRect Comet::GetRect()
{

	m_Rect = m_Sprite.getGlobalBounds();
	return m_Rect;

}