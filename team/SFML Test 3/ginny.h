#pragma once
#include <SFML\Main.hpp>
#include <SFML\Window.hpp>
#include <SFML\System.hpp>
#include <SFML\Graphics.hpp>
#include "IEntity.h"

class ginny
{
public:
	ginny(sf::Keyboard* p_pxKeys,sf::Sprite* p_xSprite,int p_ScreenWidth,int p_ScreenHeight);
	~ginny();
	void Update(float p_DeltaTime);
	sf::Sprite* GetSprite();
	sf::FloatRect* GetRect();
	sf::Vector2f GetPosition();
	bool IsActive();
	EENTITYTYPE GetType();



private:
	ginny() {};
	sf::Sprite* m_pxSpite;
	sf::FloatRect* m_pxRect;
	sf::Keyboard* m_pxKeyboard;
	float m_Speed;
	int m_ScreenWidth;
	int m_ScreenHeight;
	sf::Vector2f m_Dir;
	sf::Vector2f m_Position;
	bool m_Visible;
	bool m_Active;
};