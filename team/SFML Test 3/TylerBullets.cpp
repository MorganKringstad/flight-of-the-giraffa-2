#include "TylerBullets.h"
#include <SFML\Main.hpp>
#include <SFML\Window.hpp>
#include <SFML\System.hpp>
#include <SFML\Graphics.hpp>

TylerBullets::TylerBullets(sf::RectangleShape p_xBox, sf::RenderWindow &window, sf::Sprite p_Sprite)
{
	m_xBox = p_xBox;
	m_Sprite = p_Sprite;
	IsAlive = true;
	rect.setFillColor(sf::Color::Red);
	rect.setSize(sf::Vector2f(30, 15));
	mousePos2 = window.mapPixelToCoords(sf::Mouse::getPosition(window));

}
TylerBullets::~TylerBullets()
{

}
sf::RectangleShape TylerBullets::GetBox()
{
	return m_xBox;
}
sf::FloatRect TylerBullets::GetRect()
{
	m_Rect = rect.getGlobalBounds();
	return m_Rect;
}
void TylerBullets::SetAngle(float newAngle)
{
	m_Angle = newAngle;
}
sf::Sprite TylerBullets::GetSprite()
{

	return m_Sprite;
}
void TylerBullets::Update(float p_DeltaTime, sf::RenderWindow &window, sf::Vector2f tylerPos)
{
	m_Sprite.setPosition(rect.getPosition());
	float angle2 = 1.0f;

	

	rect.move(cos(m_Angle)*12.0f, sin(m_Angle)*12.0f);


}
bool TylerBullets::GetIsAlive() { return IsAlive; }
void TylerBullets::SetIsAlive(bool Set_IsAlive) { IsAlive = Set_IsAlive; }